const GJV = require('geojson-validation');
const _ = require('lodash');
const TurfHelpers = require('@turf/helpers');
const turfContains = require('@turf/boolean-contains').default;
const centerOfMass = require('@turf/center-of-mass').default;
const mongoose = require('mongoose');

const WeatherArea = require('../models/weatherArea');
const Helpers = require('./helpers');
const apiOpenWeather = require('../apis/openWeather');
const { mapEdges, maxSlotSideLength } = require('../config.json');

/*
NOTE: To reduce number of calls to openweather the map has been divided in slots (5x5 km aprox), The slot is a valid geojson geometry (Polygon)
openWeather allows 60 calls max/minute and 1,000,000 per month (around 30,000 per day) and the map has an aprox area of 200,000 squared km therefore to cover all area we would need around 9,000 calls per day.
*/
module.exports = class {
  /*
  Returns a map slot(geojson polygon) for the given coordinates
  @requires: geojson-validation, Helpers, lodash, #validateRectangle and #validateGetSlotArgs
  @params:
    lat, lon: number with latitude and longitude respectively
    map: Polygon that describes the map (must be a rectangle)
    maxSlot: Maximun length of the slot side (slotSideLength is calculated as the closest number to maxSlot that is a divider of the mapSize)
  @returns: Polygon representing a slot that contains the given coordinates
  */
  static getSlot(lat, lon, map, maxSlot) {
    //Validate arguments
    this.#validateRectangle(map);
    this.#validateGetSlotArgs(lat, lon, map, maxSlot);

    //Drop last point in coordinates as it is repetead
    const mapCoordinates = _.dropRight(_.head(map.coordinates));

    //Create system coordinates with axis parallel to map sides
    let slopeX = Helpers.getVectorSlope(mapCoordinates[0], mapCoordinates[1]);
    if (slopeX === Math.abs(Math.PI / 2)) {
      slopeX = 0;
    }
    const translator = Helpers.createVirtualCoordinates(
      mapCoordinates[0],
      slopeX
    );

    //Create new virtual map with sides parallel to axis
    const virtualMap = mapCoordinates.map(point =>
      translator.getVirtual(point)
    );

    //Get axes length
    const axisXLength = Helpers.getVectorLength(virtualMap[0], virtualMap[1]);
    const axisYLength = Helpers.getVectorLength(virtualMap[1], virtualMap[2]);

    // Get number of slots for each side. Slots number much fit each side by reducing the slot length to the closest number of maxSlot. Using Math.ceil for that purpose
    const slotsInX = Math.ceil(axisXLength / maxSlot);
    const slotsInY = Math.ceil(axisYLength / maxSlot);

    // Get slot X and Y real size
    const slotXlength = axisXLength / slotsInX;
    const slotYlength = axisYLength / slotsInY;

    //get virtual point from lat lon
    const virtualPoint = translator.getVirtual([lon, lat]);

    //Create slot corners
    const slotMinX = Math.floor(virtualPoint[0] / slotXlength) * slotXlength;
    let slotMaxX = Math.ceil(virtualPoint[0] / slotXlength) * slotXlength;
    const slotMinY = Math.floor(virtualPoint[1] / slotYlength) * slotYlength;
    let slotMaxY = Math.ceil(virtualPoint[1] / slotYlength) * slotYlength;
    //NOTE: top and right boundaries of slots are not inclusive. if a point falls into slot boundary it will be in the bottom or left boundary
    if (slotMinX === slotMaxX) {
      slotMaxX = slotMaxX + slotXlength;
    }
    if (slotMinY === slotMaxY) {
      slotMaxY = slotMaxY + slotYlength;
    }

    const slotLinearRing = [
      [slotMinX, slotMinY],
      [slotMaxX, slotMinY],
      [slotMaxX, slotMaxY],
      [slotMinX, slotMaxY],
      [slotMinX, slotMinY]
    ];
    return {
      type: 'Polygon',
      coordinates: [slotLinearRing.map(point => translator.getOriginal(point))]
    };
  }

  // Used by getSlot
  static #validateRectangle = rectangle => {
    if (!GJV.isPolygon(rectangle)) {
      throw new Error('rectangle is not a valid geojson polygon');
    }
    const { coordinates } = rectangle;
    if (coordinates.length > 1) {
      throw new Error('rectangle contains more than one LinearString');
    }
    const rectangleCoords = _.head(coordinates);
    if (!Helpers.isRectangle(rectangleCoords)) {
      throw new Error('rectangle must have only 90 degrees angles');
    }
  };

  // Used by getSlot
  static #validateGetSlotArgs = (lat, lon, map, maxSlot) => {
    const mapCoords = _.head(map.coordinates);
    const mapAlength = Helpers.getVectorLength(mapCoords[0], mapCoords[1]);
    const mapBlength = Helpers.getVectorLength(mapCoords[1], mapCoords[2]);
    if (maxSlot <= 0 || maxSlot > mapAlength || maxSlot > mapBlength) {
      throw new Error('Wrong slot side length');
    }
    const point = TurfHelpers.point([lon, lat]);
    if (!turfContains(map, point)) {
      throw new Error('Point coordinates are not in map');
    }
  };

  /*
  Find weather area in database
  @requires: WeatherArea moongose model (Must be connected to DB)
  @params:
    slot: Geojson polygon, (rectangle with one LinearRign)
  @returns: promise that resolves to WeatherArea or reject throwing error
  */
  static async findLocalWeatherArea(gridIndex) {
    if (!mongoose.connection.readyState) {
      throw new Error(
        `Incorrect mongoose ready state: ${mongoose.connection.readyState}`
      );
    }
    const result = await WeatherArea.findOne({ grid: gridIndex });
    return result;
  }

  /*
  Save weather area in database
  @requires: moongose model (Must be connected to DB)
  @params:
    WeatherArea: Must be a valid weatherArea
  @returns: boolean ()
  */
  static async saveWeatherArea(weatherArea) {
    const { grid, list } = weatherArea;
    try {
      if (!mongoose.connection.readyState) {
        throw new Error(
          `Incorrect mongoose ready state: ${mongoose.connection.readyState}`
        );
      }
      let result = await WeatherArea.findOneAndUpdate(
        { grid },
        { list },
        { new: true }
      );
      if (!result) {
        result = new WeatherArea(weatherArea);
        await result.save();
      }
      return result;
    } catch (e) {
      console.error('WeatherArea not saved\n', e.message);
    }
  }

  /*
  Finds a weatherArea and returns it
  @requires: geojson-validation, @turf/center-of-mass, apiOpenWeahter
  @params:
    area: geoJson type point or polygon
  @returns: WeatherArea
  Note that returns only one weather area. If area argument is a polygon that matches 2 or more slots. It will return the weather area where is the center
  */
  static async getWeatherArea(area) {
    const slotSize = Number(maxSlotSideLength);
    const { north: n, south: s, west: w, east: e } = _.mapValues(
      mapEdges,
      val => Number(val)
    );
    const map = {
      type: 'Polygon',
      coordinates: [
        [
          [w, s],
          [e, s],
          [e, n],
          [w, n],
          [w, s]
        ]
      ]
    };
    //Area must be a Point or Polygon (with only 1 linearring)
    let coords;
    if (GJV.isPoint(area)) {
      coords = [...area.coordinates];
    }
    if (GJV.isPolygon(area) && area.coordinates.length === 1) {
      coords = centerOfMass(area).geometry.coordinates;
    }
    if (!coords) {
      const error = new Error('Invalid area');
      error.code = 400;
      throw error;
    }
    // NOTE: getSlot first parameters is latitud (Y axis)
    const slot = this.getSlot(coords[1], coords[0], map, slotSize);
    const grid = Helpers.getGridIndex(slot);
    let weatherArea;
    try {
      weatherArea = await this.findLocalWeatherArea(grid);
    } catch (e) {
      console.error(e.message);
    }
    const todayStartingTime = Helpers.getToday();
    if (_.isNil(weatherArea) || weatherArea.updatedAt < todayStartingTime) {
      try {
        const list = await apiOpenWeather.getWeatherFromRemote(
          ...centerOfMass(slot).geometry.coordinates.reverse()
        );
        weatherArea = {
          grid,
          list
        };
        weatherArea = await this.saveWeatherArea(weatherArea);
      } catch (e) {
        console.error('Couldn\'t get "list" from openWEather API\n', e.message);
      }
    }
    // Due to openWeather limitations (60 calls max in 1 min) there is a chance that weatherArea is not returned
    if (_.isNil(weatherArea)) {
      const error = new Error('Weather area not available, try later');
      error.code = 500;
      throw error;
    }
    // remove internal fields from DB
    const geometry = Helpers.getGridGeometry(weatherArea.grid);
    const list = weatherArea.list.map(day =>
      _.pick(day, [
        'temp',
        'wind',
        'main',
        'dt',
        'sunrise',
        'sunset',
        'clouds',
        'rain',
        'snow'
      ])
    );
    return { geometry, list, id: weatherArea._id };
  }
};
