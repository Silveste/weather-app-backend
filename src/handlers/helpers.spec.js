const Helpers = require('./helpers');
const { mapEdges } = require('../config.json');

describe('Validate coordinates - Function valCoord', () => {
  test.each([
    ['+35.3247985', 4, 35.3248],
    ['-35.3267985', 2, -35.33],
    ['+35.3242185', 1, 35.3],
    ['-35.3242285', 0, -35],
    ['5', 3, 5.0],
    ['35.3247985', 4, 35.3248]
  ])(
    'Valid string: %s, returns a float number rounded to %i decimals: %f',
    (argument, decimalDigits, expected) => {
      const result = Helpers.valCoord(argument, decimalDigits);
      expect(result).toBe(expected);
    }
  );
  test.each([
    ['3+5.3247985'],
    ['-+35.3247985'],
    ['+-35.3242985'],
    ['-35.324.2985'],
    ['35.3247985+'],
    ['353247985'],
    ['a35.3247985'],
    ['35.3247985a'],
    ['abc']
  ])('Invalid string: %s, retuns NaN', argument => {
    const result = Helpers.valCoord(argument);
    expect(result).toBeNaN();
  });
});
describe('Coordinates point to position inside map - Function isInMap', () => {
  const { north, south, west, east } = mapEdges;
  const nBound = parseFloat(north);
  const sBound = parseFloat(south);
  const wBound = parseFloat(west);
  const eBound = parseFloat(east);
  test.each([
    [nBound - 0.001, wBound + 0.001],
    [sBound + 0.001, eBound - 0.001],
    [(nBound + sBound) / 2, (wBound + eBound) / 2]
  ])('Returns true if position lat: %f lon: %f exists in map', (lat, lon) => {
    const geometry = { type: 'Point', coordinates: [lon, lat] };
    expect(Helpers.isInMap(geometry)).toBe(true);
  });
  test.each([
    [nBound, (wBound + eBound) / 2],
    [(nBound + sBound) / 2, wBound],
    [sBound, (wBound + eBound) / 2],
    [(nBound + sBound) / 2, eBound]
  ])(
    'Returns false if position lat: %f lon: %f doesn\'t exist in map',
    (lat, lon) => {
      const geometry = { type: 'Point', coordinates: [lon, lat] };
      expect(Helpers.isInMap(geometry)).toBe(false);
    }
  );
});
describe('Query must be decoded - Function decoder', () => {
  test('Restore spaces when string contains + or %20', () => {
    const argument = 'This%20is+the%20string+to test';
    const expected = 'This is the string to test';
    expect(Helpers.decoder(argument)).toBe(expected);
  });
  test('Decodes especial characters', () => {
    const argument =
      'This%20is%20a%20string%20with%20special%20chars%20%C3%B1%2C%3A%C3%A7%C3%A1%2F-S';
    const expected = 'This is a string with special chars ñ,:çá/-S';
    expect(Helpers.decoder(argument)).toBe(expected);
  });
  test('Decodes different alphabets', () => {
    const expected = 'Тхис ис цириллыц алпхабет';
    const argument =
      '%D0%A2%D1%85%D0%B8%D1%81%20%D0%B8%D1%81%20%D1%86%D0%B8%D1%80%D0%B8%D0%BB%D0%BB%D1%8B%D1%86%20%D0%B0%D0%BB%D0%BF%D1%85%D0%B0%D0%B1%D0%B5%D1%82';
    expect(Helpers.decoder(argument)).toBe(expected);
  });
});
describe('Get the length of a vector - getVectorLength', () => {
  test.each([
    [[-1, -1], [1, 1], Math.sqrt(2) + Math.sqrt(2)],
    [[-1, -1], [0, 0], Math.sqrt(2)],
    [[0, -1], [0, 1], 2],
    [[-1, 0], [1, 0], 2],
    [[-Math.sqrt(0.5), -Math.sqrt(0.5)], [Math.sqrt(0.5), Math.sqrt(0.5)], 2]
  ])('Start: %p, End: %p returns %f', (start, end, result) => {
    expect(Helpers.getVectorLength(start, end)).toBe(result);
  });
});
describe('Get projection (X and Y length) of a vector - getVectorSlope', () => {
  test.each([
    [[-1, -1], [1, 1], Math.PI / 4],
    [[1, 1], [-1, -1], Math.PI / 4],
    [[-1, 1], [1, -1], -Math.PI / 4],
    [[1, -1], [-1, 1], -Math.PI / 4],
    [[0, -2], [0, 0], Math.PI / 2],
    [[25.5, -5.2], [36.7, -5.2], 0]
  ])(
    '%# - Start: %p, End: %p returns %f (Radians 0 to PI)',
    (start, end, result) => {
      expect(Helpers.getVectorSlope(start, end)).toBe(result);
    }
  );
});
describe('Check if is a rectangle - Function isRectangle', () => {
  test.each([
    [
      'Unclosed rectangle (4 points)',
      [
        [0, 0],
        [5, 0],
        [5, 5],
        [0, 5]
      ]
    ],
    [
      'Unclosed (5 points)',
      [
        [0, 0],
        [5, 0],
        [5, 5],
        [2.5, 5],
        [0, 5]
      ]
    ]
  ])('Throw error if argument is not LinearRing - %s', (_, rect) => {
    expect(() => Helpers.isRectangle(rect)).toThrow();
  });
  test.each([
    [
      'square with segments paralels to axes',
      [
        [0, 0],
        [5, 0],
        [5, 5],
        [0, 5],
        [0, 0]
      ]
    ],
    [
      'square with segments perpendicular to axes',
      [
        [2.5, 0],
        [5, 2.5],
        [2.5, 5],
        [0, 2.5],
        [2.5, 0]
      ]
    ],
    [
      'rectangle with segments parallel to axes',
      [
        [1.5, 2],
        [5, 2],
        [5, 8.25],
        [1.5, 8.25],
        [1.5, 2]
      ]
    ],
    [
      'rectangle with segments not parallel to axes',
      [
        [-6, -2],
        [2, -8],
        [8, 0],
        [0, 6],
        [-6, -2]
      ]
    ]
  ])('True: is a %s', (_, rect) => {
    expect(Helpers.isRectangle(rect)).toBe(true);
  });
  test.each([
    [
      'is a triangle',
      [
        [0, 0],
        [5, 2.5],
        [0, 5],
        [0, 0]
      ]
    ],
    [
      'is a pentagon',
      [
        [0, 0],
        [4, 0],
        [6, 2.5],
        [4, 5],
        [0, 5],
        [0, 0]
      ]
    ],
    [
      'opositte segments have different length',
      [
        [0, 0],
        [4, 0],
        [4, 5],
        [0, 6],
        [0, 0]
      ]
    ],
    [
      'all angles should be 90 degrees',
      [
        [0, 0],
        [4, 2],
        [4, 8],
        [0, 6],
        [0, 0]
      ]
    ]
  ])('False: %s', (_, rect) => {
    expect(Helpers.isRectangle(rect)).toBe(false);
  });
});
describe('Check if is valid gridIndex - Function isGridIndex', () => {
  const gridIdx = '-10.00,-10.00|5.21,-10.00|5.21,5.21|-10.00,5.21';
  test.each([
    [
      'It must have 4 points',
      'it has 3 points',
      '-10.00,-10.00|5.21,5.21|-10.00,5.21'
    ],
    [
      'It must have 4 points',
      'it has 5 points',
      '-10.00,-10.00|5.21,-10.00|5.21,0.00|5.21,5.21|-10.00,5.21'
    ],
    [
      'Points are composed by 2 numbers',
      '2nd point has 1 number',
      '-10.00,-10.00|5.21|5.21,5.21|-10.00,5.21'
    ],
    [
      'Decimal indicator is a dot "."',
      '2nd point 2ndt number has no decimals',
      '-10.00,-10.00|5.21,-10|5.21,5.21|-10.00,5.21'
    ],
    [
      'Decimal indicator is a dot "."',
      '2nd point 1st number has a comma as separator',
      '-10.00,-10.00|5,21,-10.00|5.21,5.21|-10.00,5.21'
    ],
    [
      'All numbers have a fixed number of decimals',
      '2nd point, 2nd number has 1 decimal',
      '-10.00,-10.00|5.21,-10.0|5.21,5.21|-10.00,5.21'
    ],
    [
      'All numbers have a fixed number of decimals',
      '3rd point, 1st number has 3 decimals',
      '-10.00,-10.00|5.21,-10.00|5.210,5.21|-10.00,5.21'
    ],
    [
      'Prec defines the number of decimals',
      'prec is 1 but numbers have 2 decimals',
      '-10.00,-10.00|5.21,-10.00|5.21,5.21|-10.00,5.21',
      '|',
      1
    ],
    [
      'Sep defines the separator',
      'Sep is "/"',
      '-10.00,-10.00|5.21,-10.00|5.21,5.21|-10.00,5.21',
      '/'
    ],
    [
      'Sep defines the separator',
      '2nd separator doesn\'t match sep',
      '-10.00,-10.00|5.21,-10.00*5.21,5.21|-10.00,5.21'
    ],
    [
      'No trailing separators',
      'It starts with a separator',
      '|-10.00,-10.00|5.21,-10.00|5.21,5.21|-10.00,5.21'
    ],
    [
      'No trailing separators',
      'It ends with a separator',
      '-10.00,-10.00|5.21,-10.00|5.21,5.21|-10.00,5.21|'
    ],
    [
      'It represents a rectangle',
      'shape is not a rectangle',
      '-10.00,-10.00|5.21,-10.00|5.21,15.21|-10.00,5.21'
    ]
  ])(
    '%# - %s, Return false because %s',
    (_str1, _str2, gridIdx, sep = '|', prec = 2) => {
      expect(Helpers.isGridIndex(gridIdx, sep, prec)).toBeFalse();
    }
  );
  test.each([
    [
      'Accepts negative numbers',
      '-10.00,-10.00|-50.00,-10.00|-50.00,-50.00|-10.00,-50.00'
    ],
    [
      'Accepts positive numbers',
      '10.00,10.00|50.00,10.00|50.00,50.00|10.00,50.00'
    ],
    [
      'Accepts positive and negative numbers',
      '-10.00,-10.00|5.21,-10.00|5.21,5.21|-10.00,5.21'
    ],
    [
      'Separator can be changed',
      '-10.00,-10.00/5.21,-10.00/5.21,5.21/-10.00,5.21',
      '/'
    ],
    [
      'Number decimals can be changed',
      '-10.0,-10.0|5.2,-10.0|5.2,5.2|-10.0,5.2',
      '|',
      1
    ]
  ])('%# - %s, Return true', (_, gridIdx, sep = '|', prec = 2) => {
    expect(Helpers.isGridIndex(gridIdx, sep, prec)).toBeTrue();
  });
  test.each([
    ',',
    '.',
    '-',
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    null,
    ''
  ])('%# - Throws error if sep is "%s" character', sep => {
    expect(() => Helpers.isGridIndex(gridIdx, sep)).toThrow();
  });
  test('Throws error if sep is more than one character', () => {
    expect(() => Helpers.isGridIndex(gridIdx, '||')).toThrow();
  });
  test.each([0, 6, -1, null])('%# - Throws error if prec is %p', prec => {
    expect(() => Helpers.isGridIndex(gridIdx, '|', prec)).toThrow();
  });
  test('Sep and prec defaults are "|" and 2 respectively', () => {
    const gridIdx = '-10.00,-10.00|5.21,-10.00|5.21,5.21|-10.00,5.21';
    expect(() =>
      Helpers.isGridIndex(gridIdx, undefined, undefined)
    ).not.toThrow();
    expect(Helpers.isGridIndex(gridIdx, undefined, undefined)).toBeTrue();
  });
});
describe('Check if is valid grid geometry - Function isGridGeometry', () => {
  const geometry = {
    type: 'Polygon',
    coordinates: [
      [
        [0.0, 0.0],
        [100.0, 0.0],
        [100.0, 100.0],
        [0.0, 100.0],
        [0.0, 0.0]
      ]
    ]
  };
  test.each([
    [
      'Is type polygon',
      'is invalid type',
      {
        type: 'MultiPolygon',
        coordinates: [
          [
            [
              [0.0, 0.0],
              [100.0, 0.0],
              [100.0, 100.0],
              [0.0, 100.0],
              [0.0, 0.0]
            ]
          ]
        ]
      }
    ],
    [
      'Is type polygon',
      'coordinates do not match type',
      {
        type: 'Polygon',
        coordinates: [
          [
            [
              [0.0, 0.0],
              [100.0, 0.0],
              [100.0, 100.0],
              [0.0, 100.0],
              [0.0, 0.0]
            ]
          ]
        ]
      }
    ],
    [
      'It must contain 1 linear ring',
      'contains 2 linear rings',
      {
        type: 'Polygon',
        coordinates: [
          [
            [0.0, 0.0],
            [100.0, 0.0],
            [100.0, 100.0],
            [0.0, 100.0],
            [0.0, 0.0]
          ],
          [
            [20.0, 20.0],
            [80.0, 20.0],
            [80.0, 80.0],
            [20.0, 80.0],
            [20.0, 20.0]
          ]
        ]
      }
    ],
    [
      'LinearRing must have 5 points',
      'it has 4 points',
      {
        type: 'Polygon',
        coordinates: [
          [
            [0.0, 0.0],
            [100.0, 0.0],
            [0.0, 100.0],
            [0.0, 0.0]
          ]
        ]
      }
    ],
    [
      'LinearRing must have 5 points',
      'it has 6 points',
      {
        type: 'Polygon',
        coordinates: [
          [
            [0.0, 0.0],
            [100.0, 0.0],
            [100.0, 100.0],
            [50.0, 100.0],
            [0.0, 100.0],
            [0.0, 0.0]
          ]
        ]
      }
    ]
  ])('%# - %s, Return false because %s', (_str1, _str2, geometry) => {
    expect(Helpers.isGridGeometry(geometry)).toBeFalse();
  });
  test('If it is polygon with one LinearRing and 5 points, Return true', () => {
    expect(Helpers.isGridGeometry(geometry)).toBeTrue();
  });
});
describe('Get gridIndex from geometry - Function getGridIndex', () => {
  const gridIdx = '0.00,0.00|100.00,0.00|100.00,100.00|0.00,100.00';
  test('Creates grid Index with sep "|" and numbers with 2 decimals by default', () => {
    const geometry = {
      type: 'Polygon',
      coordinates: [
        [
          [0.0, 0.0],
          [100.0, 0.0],
          [100.0, 100.0],
          [0.0, 100.0],
          [0.0, 0.0]
        ]
      ]
    };
    expect(Helpers.getGridIndex(geometry)).toBe(gridIdx);
  });
  test('Numbers are rounded to the correct number', () => {
    const gridIdx = '0.00,0.00|100.51,0.00|100.51,100.51|0.00,100.51';
    const geometry = {
      type: 'Polygon',
      coordinates: [
        [
          [0, 0.0],
          [100.50666, 0.0],
          [100.50666, 100.50666],
          [0.0, 100.50666],
          [0.0, 0.0]
        ]
      ]
    };
    expect(Helpers.getGridIndex(geometry)).toBe(gridIdx);
  });
  test('Throws error if geometry is not valid index geometry', () => {
    const geometry = {
      type: 'Polygon',
      coordinates: [
        [
          [0, 0.0],
          [100.0, 50.0], //Invalid index geometry because is not a rectangle
          [100.0, 100.0],
          [0.0, 100.0],
          [0.0, 0.0]
        ]
      ]
    };
    expect(() => Helpers.getGridIndex(geometry)).toThrow();
  });
  test('It acepts custom separators', () => {
    const gridIdx = '0.00,0.00/100.00,0.00/100.00,100.00/0.00,100.00';
    const geometry = {
      type: 'Polygon',
      coordinates: [
        [
          [0, 0.0],
          [100.0, 0.0],
          [100.0, 100.0],
          [0.0, 100.0],
          [0.0, 0.0]
        ]
      ]
    };
    expect(Helpers.getGridIndex(geometry, '/')).toBe(gridIdx);
  });
  test.each([
    '.',
    ',',
    '-',
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    null,
    ''
  ])('%# - Throws an error if separator character is: %p', sep => {
    const geometry = {
      type: 'Polygon',
      coordinates: [
        [
          [0, 0.0],
          [100.0, 0.0],
          [100.0, 100.0],
          [0.0, 100.0],
          [0.0, 0.0]
        ]
      ]
    };
    expect(() => Helpers.getGridIndex(geometry, sep)).toThrow();
  });
  test('Decimal precision can be adjusted', () => {
    const gridIdx = '0.000,0.000|100.000,0.000|100.000,100.000|0.000,100.000';
    const geometry = {
      type: 'Polygon',
      coordinates: [
        [
          [0, 0.0],
          [100.0, 0.0],
          [100.0, 100.0],
          [0.0, 100.0],
          [0.0, 0.0]
        ]
      ]
    };
    expect(Helpers.getGridIndex(geometry, '|', 3)).toBe(gridIdx);
  });
  test.each([-1, 0, 6, 'a'])(
    'Only prec values [1-5] are valid. Throws error because value: %p',
    prec => {
      const geometry = {
        type: 'Polygon',
        coordinates: [
          [
            [0, 0.0],
            [100.0, 0.0],
            [100.0, 100.0],
            [0.0, 100.0],
            [0.0, 0.0]
          ]
        ]
      };
      expect(() => Helpers.getGridIndex(geometry, '|', prec)).toThrow();
    }
  );
});
describe('Get geometry from gridIndex - Function getGeometry', () => {
  const validGridIdx = '0.00,0.00|100.00,0.00|100.00,100.00|0.00,100.00';
  const validGeometry = {
    type: 'Polygon',
    coordinates: [
      [
        [0, 0.0],
        [100.0, 0.0],
        [100.0, 100.0],
        [0.0, 100.0],
        [0.0, 0.0]
      ]
    ]
  };
  test('Returns a valid grid geometry', () => {
    expect(Helpers.getGridGeometry(validGridIdx)).toEqual(validGeometry);
  });
  test.each([
    [validGridIdx, '.', 2],
    [validGridIdx, ',', 2],
    [validGridIdx, '-', 2],
    [validGridIdx, '0', 2],
    [validGridIdx, '1', 2],
    [validGridIdx, '2', 2],
    [validGridIdx, '3', 2],
    [validGridIdx, '4', 2],
    [validGridIdx, '5', 2],
    [validGridIdx, '6', 2],
    [validGridIdx, '7', 2],
    [validGridIdx, '8', 2],
    [validGridIdx, '9', 2],
    [validGridIdx, '|', -1],
    [validGridIdx, '|', 0],
    [validGridIdx, '|', 6],
    [validGridIdx, '/', 2], //sep is val however doesn't match the grid index
    [validGridIdx, '|', 3], //prec is val however doesn't match the grid index
    ['0.00,0.00|100.00,50.00|100.00,100.00|0.00,100.00', '|', 2] //gridIdx is not a rectangle
  ])(
    '%# - Throws error if gridIdx, sep or prec are not valid',
    (gIdx, s, p) => {
      expect(() => Helpers.getGridGeometry(gIdx, s, p)).toThrow();
    }
  );
});
describe('Get slope of a vector - Function getVectorSlope', () => {
  test('Returns slope in radians', () => {
    const result = Helpers.getVectorSlope([1, 1], [3, 3]);
    expect(result).toBe(Math.PI / 4);
  });
  test.each([
    [
      [3, 6],
      [3, 12]
    ],
    [
      [5, -1],
      [5, -20]
    ]
  ])(
    '%# Vertical vector (parallel to Y axis) is always postive (PI/2)',
    (start, end) => {
      const result = Helpers.getVectorSlope(start, end);
      expect(result).toBe(Math.PI / 2);
    }
  );
  test.each([
    [
      [5, 6],
      [20, 6]
    ],
    [
      [3, 0],
      [-2.5, 0]
    ]
  ])('%# Vector with no slope (parallel to X axis) is Zero', (start, end) => {
    const result = Helpers.getVectorSlope(start, end);
    expect(result).toBe(0);
  });
  test.each([
    [
      [3, 5],
      [3, 5]
    ],
    [
      [0, 0],
      [0, 0]
    ]
  ])('Throws error if vector has no length', (start, end) => {
    expect(() => Helpers.getVectorSlope(start, end)).toThrow();
  });
});
describe('Get virtual coordinateSystem - Function createVirtualCoordinates', () => {
  describe('Validate params', () => {
    test.each([
      ['is null', null],
      ['is not an array', 6],
      ['length is not 2', [5.6, 3.2, 7.8]],
      ['length is not 2', [5.6]],
      [' contains NaN', ['a', 5]],
      [' contains NaN', ['a', 'b']],
      [' contains NaN', [5, 'a']]
    ])('%# - Throw error if origin %s', (_, origin) => {
      expect(() => Helpers.createVirtualCoordinates(origin)).toThrow();
    });
    test.each([
      ['is NaN', 'a'],
      ['is equal than PI/2', Math.PI / 2],
      ['is bigger than PI/2', 1 + Math.PI / 2],
      ['is equal than -PI/2', -Math.PI / 2],
      ['is smaller than PI/2', -Math.PI / 2 - 1]
    ])('%# Throw error if slope %s', (_, slope) => {
      expect(() => Helpers.createVirtualCoordinates([0, 0], slope)).toThrow();
    });
  });
  describe('Returning functions: getOriginal, getVirtual', () => {
    test.each([
      ['is null', null],
      ['is not an array', 6],
      ['length is not 2', [5.6, 3.2, 7.8]],
      ['length is not 2', [5.6]],
      [' contains NaN', ['a', 5]],
      [' contains NaN', ['a', 'b']],
      [' contains NaN', [5, 'a']]
    ])('%# - Throw error if point %s', (_, origin) => {
      const translator = Helpers.createVirtualCoordinates([0, 0], 0);
      expect(() => translator.getOriginal(origin)).toThrow();
      expect(() => translator.getVirtual(origin)).toThrow();
    });
    describe('Returned value', () => {
      test('Is a point', () => {
        /*Array of length 2 with 2 numbers*/
        const translator = Helpers.createVirtualCoordinates([0, 0], 0);
        const originalValue = translator.getOriginal([0, 0]);
        const virtualValue = translator.getVirtual([0, 0]);
        expect(originalValue).toBeArrayOfSize(2);
        expect(virtualValue).toBeArrayOfSize(2);
        expect(originalValue).toIncludeSameMembers([
          expect.any(Number),
          expect.any(Number)
        ]);
        expect(virtualValue).toIncludeSameMembers([
          expect.any(Number),
          expect.any(Number)
        ]);
      });
      test.each([
        [[0, 0], [0, 0], [0, 0], 0],
        [[-4, -5], [0, 0], [-4, -5], 0],
        [[0, 0], [5.2, -3.6], [-5.2, 3.6], 0],
        [[15.5, 11.3], [15, 14.9], [0.5, -3.6], 0],
        [[0, 0], [0, 0], [0, 0], Math.PI / 4],
        [[3.535534, 3.535534], [5, 0], [0, 0], Math.PI / 4],
        [[-3.535534, 3.535534], [0, 5], [0, 0], Math.PI / 4],
        [[-3.535534, -3.535534], [-5, 0], [0, 0], Math.PI / 4],
        [[3.535534, -3.535534], [0, -5], [0, 0], Math.PI / 4],
        [
          [-15.728915, 8.898018],
          [5.638156, 2.052121],
          [-20.45698, 5.20405],
          Math.PI / 10
        ],
        [[32, -19], [-0.448342, 6.308644], [30, -25], -Math.PI / 8]
      ])(
        '%# - Original point: %p is equivalent to Virtual point: %p - Origin: %p, Slope: %f',
        (originalP, virtualP, virtualOrigin, slope) => {
          const translator = Helpers.createVirtualCoordinates(
            virtualOrigin,
            slope
          );
          const originalValue = translator.getOriginal(virtualP);
          const virtualValue = translator.getVirtual(originalP);
          expect(originalValue[0]).toBeCloseTo(originalP[0], 5);
          expect(originalValue[1]).toBeCloseTo(originalP[1], 5);
          expect(virtualValue[0]).toBeCloseTo(virtualP[0], 5);
          expect(virtualValue[1]).toBeCloseTo(virtualP[1], 5);
        }
      );
    });
  });
});
describe('Get the time when today started - getToday', () => {
  test('Returns Date instance of today at 0h:00m:00s:0000ms', () => {
    const result = Helpers.getToday().valueOf();
    const now = new Date().valueOf();
    const msPerDay = 60 * 60 * 24 * 1000;
    expect(now - result).toBeLessThan(msPerDay);
    expect(result % msPerDay).toBe(0);
  });
});
