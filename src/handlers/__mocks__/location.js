const LocationHandlers = class {};

LocationHandlers.getCity = jest.fn((query) => {
  return new Promise((resolve, reject) => {
    const map = {
      'dublin': { lat: '53.3498', lon: '-6.2603'},
      'galway': { lat: '53.2709', lon: '-9.0626'},
      'cork': { lat: '51.9036', lon: '-8.4683'},
      'reject_by_weatherarea': { lat: '53.5000', lon: '-6.5000'},
    };
    const key = query.toLowerCase();
    if (map[key]){
      const result = {
        code: 200,
        locations: [{
          message: 'This object comes from a mock function',
          geometry: {
            type: 'Point',
            coordinates: [Number(map[key].lon), Number(map[key].lat)]
          }
        }]
      };
      resolve(result);
    } else {
      const error = new Error('Not found');
      error.code = 404;
      reject(error);
    }
  });
});

LocationHandlers.getCoords = jest.fn((lat, lon) => {
  return new Promise((resolve, reject) => {
    const map = {
      '53.3498-6.2603': {city: 'Dublin'},
      '53.2709-9.0626': {city: 'Galway'},
      '51.9036-8.4683': {city: 'Cork'},
      '53.5000-6.5000': {city: 'Cords should be rejected by weatherArea mock'}
    };
    const key = lat.toString() + lon.toString();
    if (map[key]){
      const result = {
        code: 200,
        locations: [{
          message: 'This object comes from a mock function',
          ...map[key]
        }]
      };
      resolve(result);
    } else if ( key === '53.4000-6.4000' ) {
      const error = new Error('Invalid Area');
      error.code = 400;
      reject(error);
    } else if ( key === '53.404-6.404' ) {
      const error = new Error('Not found');
      error.code = 404;
      reject(error);
    } else {
      // The mock will set the error code passed through the first parameter
      const error = new Error('Error message from mock');
      error.code = parseInt(lat);
      reject(error);
    }
  });
});

module.exports = LocationHandlers;
