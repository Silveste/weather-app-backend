const WeatherHandlers = class {};

WeatherHandlers.getWeatherArea = jest.fn((geoJson) => {
  return new Promise((resolve) => {
    const error500 = new Error('reached openWeather limit');
    error500.code = 500;
    const error400 = new Error('Invalid Area');
    error400.code = 400;
    const coords = geoJson.coordinates;
    const key = `${coords[0]},${coords[1]}`;
    switch (key){
    case '-6.2603,53.3498':
    case '-6.404,53.404': // Should triger 404 not found error in getCoords
      resolve({geometry: {}, list: [{message: 'This object comes from a mock function'}]});
      break;
    case '-6.4,53.4':
      throw(error400);
    default:
      throw(error500);
    }
  });
});

module.exports = WeatherHandlers;
