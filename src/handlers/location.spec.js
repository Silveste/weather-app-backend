const LocationHandlers = require('./location');
//const Helpers = require('./helpers');
const _ = require('lodash');
const GJV = require('geojson-validation');
const { featureTypes } = require('../config.json');
const mongoose = require('mongoose');
const LocationModel = require('../models/location');
const locationData = require('../models/__mocks__/location-mock-data.json');
const mockFeaturesRaw = require('./__mocks__/raw-features.json');
const mockFeaturesFiltered = require('./__mocks__/filtered-features.json');
const mockFeaturesMerged = require('./__mocks__/merged-features.json');
jest.mock('../apis/nominatim');
const { findRemoteCity } = require('../apis/nominatim');

describe('DB queries', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      {
        bufferCommands: false,
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
      },
      err => {
        if (err) throw new Error(err);
      }
    );
  });
  beforeEach(async () => {
    await LocationModel.deleteMany();
    await LocationModel.collection.insertMany(locationData);
    await LocationModel.syncIndexes();
  });
  afterAll(async () => {
    await mongoose.disconnect();
  });
  describe('Query describing place as a string - Function findLocalCity', () => {
    describe('Found - Returns array of locations', () => {
      test.each([
        ['dublin', 4],
        ['GalwAy', 1],
        ['dundAlk louth', 2],
        ['duBlin galway LoutH', 7]
      ])(
        '"%s" returns array of length %i with valid docs',
        async (query, resultLength) => {
          const result = await LocationHandlers.findLocalCity(query, 0);
          const validationsResult = await Promise.all(
            result.map(val => LocationModel.validate(val))
          );
          expect(result).toBeArrayOfSize(resultLength);
          expect(result).toSatisfyAll(
            loc => !(loc instanceof mongoose.Document)
          );
          expect(validationsResult).toSatisfyAll(
            el => typeof el === 'undefined'
          );
        }
      );
      test.each([
        [3, 1],
        [2.35, 2],
        [1.5, 3],
        [1, 4],
        [0, 4]
      ])(
        'Returns array of objects where db score >= %f. Score is normalized',
        async (minScore, resultLength) => {
          const result = await LocationHandlers.findLocalCity(
            'dublin',
            minScore
          );
          expect(result).toBeArrayOfSize(resultLength);
          result.forEach(el => {
            expect(el.score).toBeGreaterThanOrEqual(1);
          });
        }
      );
    });
    describe('Not found - Returns error not found', () => {
      test('Error message - Not found in database', async () => {
        expect.assertions(1);
        await expect(
          LocationHandlers.findLocalCity('cannotBeFound', 0)
        ).rejects.toThrow('Not found in database');
      });
    });
  });
  describe('Query using coords - Function findLocalCoords', () => {
    describe('Found - Returns array of locations', () => {
      test.each([
        [7.5, 7.5, 2],
        [2.5, -15, 1]
      ])(
        'Lat: %f, Lon: %f returns array of length %i with valid docs',
        async (lat, lon, resultLength) => {
          const result = await LocationHandlers.findLocalCoords(lat, lon);
          const validationsResult = await Promise.all(
            result.map(val => LocationModel.validate(val))
          );
          expect(result).toBeArrayOfSize(resultLength);
          expect(result).toSatisfyAll(
            loc => !(loc instanceof mongoose.Document)
          );
          expect(validationsResult).toSatisfyAll(
            el => typeof el === 'undefined'
          );
        }
      );
      test('If bestMatch = true returns the smaller polygon that intersects', async () => {
        const result = await LocationHandlers.findLocalCoords(7.5, 7.5, true);
        expect(result).toBeArrayOfSize(1);
        expect(result).toSatisfyAll(loc => !(loc instanceof mongoose.Document));
        expect(_.head(result).name).toBe('Howth'); // Howth is smaller than Dublin
        const validation = await LocationModel.validate(_.head(result));
        expect(validation).toBeUndefined();
      });
    });
    describe('Not found - Returns error not found', () => {
      test('Error message - Not found in database', async () => {
        await expect(LocationHandlers.findLocalCoords(25, 25)).rejects.toThrow(
          'Not found in database'
        );
      });
    });
  });
});

describe('Methods data management', () => {
  describe('Filter Features in function of properties.type - Function filterFeatures', () => {
    const mockFeaturesCorrectGeometry = mockFeaturesRaw.filter(feature =>
      ['Polygon', 'Point'].includes(feature.geometry.type)
    );
    const mockFeaturesIncorrectGeometry = mockFeaturesRaw.filter(
      feature => !['Polygon', 'Point'].includes(feature.geometry.type)
    );
    test('Discard types not included in config.featureTypes', () => {
      const result = LocationHandlers.filterFeatures(
        mockFeaturesCorrectGeometry
      );
      _.transform(result, (acc, val) => acc.push(...val), []).forEach(
        feature => {
          expect(feature.properties.type).toBeOneOf(featureTypes);
        }
      );
    });
    test('Return object with propertiies "polygons" and "points"', () => {
      const result = LocationHandlers.filterFeatures(
        mockFeaturesCorrectGeometry
      );
      expect(result.polygons).toBeArray();
      expect(result.polygons).not.toBeEmpty();
      expect(result.polygons).toBeArray();
      expect(result.polygons).not.toBeEmpty();
    });
    test('Polygons: features with administrative type and geometry Polygon', () => {
      const { polygons } = LocationHandlers.filterFeatures(
        mockFeaturesCorrectGeometry
      );
      polygons.forEach(polygon => {
        expect(polygon.properties.type).toBe('administrative');
        expect(GJV.isPolygon(polygon.geometry)).toBe(true);
      });
    });
    test('Points: features with geometry point and not administrative', () => {
      const { points } = LocationHandlers.filterFeatures(
        mockFeaturesCorrectGeometry
      );
      points.forEach(point => {
        expect(point.properties.type).toBeOneOf(featureTypes);
        expect(point.properties.type).not.toBe('administrative');
        expect(GJV.isPoint(point.geometry)).toBe(true);
      });
    });
    test('Log warning if there is a valid type with geometry different than point or polygon', () => {
      jest.spyOn(console, 'warn').mockImplementation(() => {});
      const result = LocationHandlers.filterFeatures(
        mockFeaturesIncorrectGeometry
      );
      expect(result.polygons).toBeArrayOfSize(0);
      expect(result.points).toBeArrayOfSize(0);
      expect(console.warn).toHaveBeenCalledTimes(1);
      console.warn.mockRestore();
    });
  });
  describe('Compose Location document - function compileLocationDocuments', () => {
    let findCoordsSpy;
    beforeEach(() => {
      findCoordsSpy = jest
        .spyOn(LocationHandlers, 'findCoords')
        .mockImplementation(() => {
          return [
            {
              type: 'administrative',
              name: 'name from mock',
              county: 'county from mock',
              country: 'country from mock',
              displayName: 'displaName from mock',
              osmId: 123456,
              osmType: 'mockOsmType',
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [0, 0],
                    [0, 1],
                    [1, 1],
                    [0, 0]
                  ]
                ]
              }
            }
          ];
        });
    });
    afterEach(() => findCoordsSpy.mockRestore());
    const getByGeometry = geometry => {
      return mockFeaturesFiltered.filter(
        feature => feature.geometry.type === geometry
      );
    };
    const getByOsmIds = (...osmIds) => {
      return osmIds.map(osmId =>
        mockFeaturesFiltered.find(el => el.properties.osm_id === osmId)
      );
    };

    describe('All cases, returns array of objects', () => {
      test('It contains polygon geometry', async () => {
        const polygons = getByGeometry('Polygon');
        const points = getByGeometry('Point');
        const result = await LocationHandlers.compileLocationDocuments(
          polygons,
          points
        );
        expect(result).toSatisfyAll(item => GJV.isPolygon(item.geometry));
      });
      test('It works when polygons is null', async () => {
        const points = getByGeometry('Point');
        const result = await LocationHandlers.compileLocationDocuments(
          null,
          points
        );
        expect(result).toBeArrayOfSize(points.length);
        expect(findCoordsSpy).toHaveBeenCalledTimes(points.length);
      });
      test('Log errors when cannot map from Nominatim to Location', async () => {
        jest.spyOn(console, 'error').mockImplementation(() => null);
        const invalidNominatimObj = {
          name: 'invalid object'
        };
        const polygons = getByGeometry('Polygon');
        const points = getByGeometry('Point');
        points.push(invalidNominatimObj);
        polygons.push(invalidNominatimObj);
        await LocationHandlers.compileLocationDocuments(polygons, points);
        expect(console.error).toHaveBeenCalledTimes(2);
        console.error.mockRestore();
      });
      test('Returns an array of valid Location documents (Not mongoose document)', async () => {
        const polygons = getByGeometry('Polygon');
        const points = getByGeometry('Point');
        const result = await LocationHandlers.compileLocationDocuments(
          polygons,
          points
        );
        const promises = result.map(loc => LocationModel.validate(loc));
        const validations = await Promise.all(promises);
        expect(result).toSatisfyAll(loc => !(loc instanceof mongoose.Document));
        validations.forEach(loc => expect(loc).toBeUndefined());
      });
    });
    describe('Point feature whithin a polygon feature provided in args', () => {
      const polygonOsmIds = [4781717, 3158766, 6981937];
      const pointOsmIds = [663428001, 3316584769, 52263134];
      const polygons = getByOsmIds(...polygonOsmIds);
      const points = getByOsmIds(...pointOsmIds);
      test('Merge matching polygons, include reference to polygon feature', async () => {
        const result = await LocationHandlers.compileLocationDocuments(
          polygons,
          points
        );
        expect(findCoordsSpy).not.toBeCalled();
        result.forEach(loc => {
          expect(loc.osmId).toBeOneOf(pointOsmIds);
          expect(loc.polygonOsmId).toBeOneOf(polygonOsmIds);
          expect(loc.polygonOsmType).toBeString();
        });
      });
      test('Discard polygon location after merging', async () => {
        const result = await LocationHandlers.compileLocationDocuments(
          polygons,
          points
        );
        expect(result).toBeArrayOfSize(points.length);
        result.forEach(loc => {
          expect(loc.osmId).not.toBeOneOf(polygonOsmIds);
          expect(loc.polygonOsmId).not.toBeOneOf(pointOsmIds);
        });
      });
    });
    describe('Point feature with matching polygon provided in args', () => {
      const pointOsmIds = [
        52258370,
        2805198335,
        2795646448,
        2293453861,
        582043319
      ];
      const polygons = getByGeometry('Polygon');
      const polygonOsmIds = polygons.map(loc => loc.properties.osm_id);
      const points = getByOsmIds(...pointOsmIds);
      test('Get polygon using point coordinates', async () => {
        const result = await LocationHandlers.compileLocationDocuments(
          polygons,
          points
        );
        expect(result).toBeArrayOfSize(points.length + polygons.length);
        const pointsResult = result.filter(loc =>
          pointOsmIds.includes(loc.osmId)
        );
        //NOTE: Test will pass if function returns points in order but this is not a requirement
        pointsResult.forEach((loc, i) => {
          const originalPoint = points.find(
            point => loc.osmId === point.properties.osm_id
          );
          const crds = originalPoint.geometry.coordinates;
          expect(findCoordsSpy).toHaveBeenNthCalledWith(
            i + 1,
            crds[1],
            crds[0],
            true
          );
          expect(GJV.isPolygon(loc.geometry)).toBe(true);
        });
      });
      test('Include reference to polygon feature', async () => {
        const result = await LocationHandlers.compileLocationDocuments(
          polygons,
          points
        );
        expect(result).toBeArrayOfSize(points.length + polygons.length);
        const pointsResult = result.filter(loc =>
          pointOsmIds.includes(loc.osmId)
        );
        pointsResult.forEach(loc => {
          expect(loc).toHaveProperty('polygonOsmId', expect.any(Number));
          expect(loc).toHaveProperty('polygonOsmType', expect.any(String));
          expect(loc.polygonOsmId).not.toBeOneOf(polygonOsmIds);
        });
      });
    });
    describe('Polygon feature without matching point feature provided in args', () => {
      const points = getByGeometry('Point');
      const polygonOsmIds = [
        4636126,
        5667717,
        5242703,
        4275226,
        5449010,
        4551618,
        2314705,
        2182630,
        5409146,
        4146657,
        1109531,
        5489469,
        6142526
      ];
      const polygons = getByOsmIds(...polygonOsmIds);
      test('Include polygon as a standalone location', async () => {
        const result = await LocationHandlers.compileLocationDocuments(
          polygons,
          points
        );
        expect(result).toBeArrayOfSize(points.length + polygons.length);
        const polygonResult = result.filter(loc =>
          polygonOsmIds.includes(loc.osmId)
        );
        expect(polygonResult).toBeArrayOfSize(polygonOsmIds.length);
      });
      test('Standalone polygon location do not have polygonOsmId and poligonOsmType', async () => {
        const result = await LocationHandlers.compileLocationDocuments(
          polygons,
          points
        );
        expect(result).toBeArrayOfSize(points.length + polygons.length);
        const polygonResult = result.filter(loc =>
          polygonOsmIds.includes(loc.osmId)
        );
        polygonResult.forEach(loc => {
          expect(loc).not.toContainAnyKeys(['polygonOsmId', 'polygonOsmType']);
        });
      });
    });
  });
});

describe('Update DB - function saveLocation', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      {
        bufferCommands: false,
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
      },
      err => {
        if (err) throw new Error(err);
      }
    );
    await LocationModel.syncIndexes();
  });
  beforeEach(async () => {
    await LocationModel.deleteMany();
  });
  afterAll(async () => {
    await mongoose.disconnect();
  });
  const mockLocation = {
    type: 'administrative',
    name: 'location-name',
    county: 'county-name',
    country: 'country-name',
    displayName: 'this is a test',
    importance: 0.234,
    osmId: 111,
    osmType: 'relation',
    geometry: {
      type: 'Polygon',
      coordinates: [
        [
          [0, 0],
          [10, 5],
          [0, 10],
          [0, 0]
        ]
      ]
    }
  };
  const saveLocations = async (...locs) => {
    // Need to run in sequence as some tests depends on the DB insertions made before
    const results = [];
    for (const loc of locs) {
      const result = await LocationHandlers.saveLocation(loc);
      results.push(result);
    }
    return results.every(result => result);
  };
  const locationModelImplementation = property =>
    jest.spyOn(LocationModel, property).mockImplementation(() => {
      return { exec: () => Promise.reject(new Error('Error from test')) };
    });
  describe('Data added sucessfully, return true', () => {
    test('Save administrative location successfully', async () => {
      const success = await saveLocations(mockLocation);
      const saved = _.head(await LocationModel.find().exec());
      expect(saved.toJSON()).toMatchObject(mockLocation);
      expect(success).toBe(true);
    });
    test('Save non administrative location successfully', async () => {
      const cityMockLocation = {
        ...mockLocation,
        type: 'city',
        osmId: 1111,
        polygonOsmId: 111,
        polygonOsmType: 'relation'
      };
      const success = await saveLocations(cityMockLocation);
      const saved = _.head(await LocationModel.find().exec());
      expect(saved.toJSON()).toMatchObject(cityMockLocation);
      expect(success).toBe(true);
    });
    test('Save 2 locations successfully (same osmId different osmType)', async () => {
      const mockLocation2 = {
        ...mockLocation,
        osmType: 'different'
      };
      const success = await saveLocations(mockLocation, mockLocation2);
      const saved = await LocationModel.find({
        osmId: mockLocation.osmId
      }).exec();
      expect(saved).toBeArrayOfSize(2);
      expect(success).toBe(true);
    });
    test('Save 2 locations successfully (same osmType different osmId)', async () => {
      const mockLocation2 = {
        ...mockLocation,
        osmId: 2222
      };
      const success = await saveLocations(mockLocation, mockLocation2);
      const saved = await LocationModel.find({
        osmType: mockLocation.osmType
      }).exec();
      expect(saved).toBeArrayOfSize(2);
      expect(success).toBe(true);
    });
    test('Remove administrative location in DB used to retrieve polygon geometry', async () => {
      const locations = [
        mockLocation,
        {
          ...mockLocation,
          type: 'city',
          osmId: 1111,
          polygonOsmId: 111,
          polygonOsmType: 'relation'
        }
      ];
      const success = await saveLocations(...locations);
      const saved = await LocationModel.find().exec();
      const removed = await LocationModel.find({
        osmId: locations[1].polygonOsmId
      }).exec();
      expect(saved).toBeArrayOfSize(1);
      expect(_.head(saved).toJSON()).toMatchObject(locations[1]);
      expect(removed).toBeEmpty();
      expect(success).toBe(true);
    });
    test('polygonOsmId && polygonOsmType duplicated in other location, save when name matches polygonName', async () => {
      jest.spyOn(console, 'warn').mockImplementation(() => null);
      const locations = [
        {
          ...mockLocation,
          type: 'city',
          osmId: 1111,
          polygonOsmId: 111,
          polygonOsmType: 'relation'
        },
        {
          ...mockLocation,
          type: 'city',
          osmId: 2222,
          polygonOsmId: 111,
          polygonOsmType: 'relation',
          polygonName: 'location-name'
        }
      ];
      const success = await saveLocations(...locations);
      const saved = await LocationModel.find().exec();
      const savedObj = saved.map(loc => loc.toJSON());
      expect(savedObj).toBeArrayOfSize(1);
      const loc = _.head(savedObj);
      expect(loc.polygonOsmId).toBe(111);
      expect(loc.polygonOsmType).toBe('relation');
      expect(loc.polygonName).toBe('location-name');
      expect(loc.osmId).toBeOneOf([1111, 2222]);
      expect(console.warn).toHaveBeenCalledTimes(2);
      expect(success).toBe(true);
      console.warn.mockRestore();
    });
    test('DB query error - Location.find fails log error', async () => {
      const findMock = locationModelImplementation('find');
      const consoleErrorMock = jest
        .spyOn(console, 'error')
        .mockImplementation(() => null);
      const success = await saveLocations(mockLocation, mockLocation);
      expect(findMock).toHaveBeenCalledTimes(2);
      expect(success).toBe(false);
      expect(consoleErrorMock).toHaveBeenCalledTimes(2);
      expect(consoleErrorMock).toHaveBeenCalledWith(
        'Cannot save location:\n',
        mockLocation,
        '\nError: ',
        'Error from test'
      );
      findMock.mockRestore();
      consoleErrorMock.mockRestore();
    });
    test('DB query error - Location.findByIdAndRemove fails', async () => {
      const cityMockLocation = {
        ...mockLocation,
        type: 'city',
        osmId: 1111,
        polygonOsmId: 111,
        polygonOsmType: 'relation'
      };
      const findAndRemoveMock = locationModelImplementation(
        'findByIdAndRemove'
      );
      const consoleErrorMock = jest
        .spyOn(console, 'error')
        .mockImplementation(() => null);
      const success = await saveLocations(mockLocation, cityMockLocation);
      const saved = await LocationModel.find().exec();
      expect(saved).toBeArrayOfSize(1);
      expect(findAndRemoveMock).toHaveBeenCalledTimes(1);
      expect(success).toBe(false);
      expect(consoleErrorMock).toHaveBeenCalledTimes(1);
      expect(consoleErrorMock).toHaveBeenCalledWith(
        'Cannot save location:\n',
        expect.anything(),
        '\nError: ',
        'Error from test'
      );
      findAndRemoveMock.mockRestore();
      consoleErrorMock.mockRestore();
    });
  });
  describe('Data not added, return false', () => {
    test('location already in DB (osmId && osmType present),log warning', async () => {
      const consoleWarnMock = jest
        .spyOn(console, 'warn')
        .mockImplementation(() => null);
      const mockLocation2 = {
        ...mockLocation,
        type: 'city',
        name: 'should-not-be-recorded'
      };
      const success = await saveLocations(mockLocation, mockLocation2);
      const saved = await LocationModel.find().exec();
      expect(saved).toBeArrayOfSize(1);
      expect(console.warn).toHaveBeenCalledTimes(1);
      expect(consoleWarnMock).toHaveBeenCalledWith(
        expect.stringContaining('is already in DB as Location')
      );
      const savedLoc = _.head(saved);
      expect(savedLoc.toJSON()).toMatchObject(mockLocation);
      expect(success).toBe(false);
      consoleWarnMock.mockRestore();
    });
    test('location already in DB (polygonOsmId && polygonOsmType present),log warning', async () => {
      const consoleWarnMock = jest
        .spyOn(console, 'warn')
        .mockImplementation(() => null);
      const mockLocation1 = {
        ...mockLocation,
        osmId: 1111,
        osmType: 'city',
        polygonOsmId: 111,
        polygonOsmType: 'relation'
      };
      const success = await saveLocations(mockLocation1, mockLocation);
      const saved = await LocationModel.find().exec();
      expect(saved).toBeArrayOfSize(1);
      const savedLoc = _.head(saved);
      expect(savedLoc.toJSON()).toMatchObject(mockLocation1);
      expect(consoleWarnMock).toHaveBeenCalledTimes(1);
      expect(consoleWarnMock).toHaveBeenCalledWith(
        expect.stringContaining('is already in DB as geometry of')
      );
      expect(success).toBe(false);
      consoleWarnMock.mockRestore();
    });
    test('Invalid document, log error', async () => {
      const consoleErrorMock = jest
        .spyOn(console, 'error')
        .mockImplementation(() => null);
      const invalid = { osmId: 999, name: null };
      const success = await saveLocations(invalid);
      const notPresent = await LocationModel.find().exec();
      expect(consoleErrorMock).toHaveBeenCalledTimes(1);
      expect(consoleErrorMock).toHaveBeenCalledWith(
        'Cannot save location:\n',
        invalid,
        '\nError: ',
        expect.anything()
      );
      expect(notPresent).toBeEmpty();
      expect(success).toBe(false);
      consoleErrorMock.mockRestore();
    });
  });
});

describe('Integration test - function getCity', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
      },
      err => {
        if (err) throw new Error(err);
      }
    );
    await LocationModel.syncIndexes();
  });
  beforeEach(async () => {
    await LocationModel.deleteMany();
  });
  afterAll(async () => {
    await mongoose.disconnect();
  });
  describe('Valid arguments', () => {
    test('Return 200 and correct object', async () => {
      const testLocation = mockFeaturesMerged.find(
        el => el.name.toLowerCase() === 'dublin'
      );
      await LocationModel.create(testLocation);
      const result = await LocationHandlers.getCity('dublin');
      const {
        name,
        geometry,
        county,
        country,
        displayName,
        type,
        importance,
        osmType,
        osmId
      } = testLocation;
      expect(result).toEqual({
        code: 200,
        locations: [
          {
            id: `${osmType}-${osmId}`,
            name,
            geometry,
            county,
            country,
            displayName,
            type,
            importance,
            score: expect.toBeNumber()
          }
        ]
      });
    });
    test('Located in DB - Return scrore along location', async () => {
      const testLocation = mockFeaturesMerged.filter(el =>
        _.toLower(el.displayName).includes('cavan')
      );
      await LocationModel.insertMany(testLocation);
      const findLocalCitySpy = jest.spyOn(LocationHandlers, 'findLocalCity');
      const filterFeaturesSpy = jest.spyOn(LocationHandlers, 'filterFeatures');
      const compileLocationDocumentsSpy = jest.spyOn(
        LocationHandlers,
        'compileLocationDocuments'
      );
      const saveLocationSpy = jest
        .spyOn(LocationHandlers, 'saveLocation')
        .mockImplementation(() => null);
      const result = await LocationHandlers.getCity('corduff cavan');
      expect(findLocalCitySpy).toHaveBeenCalledWith('corduff cavan', 2);
      expect(findLocalCitySpy).toHaveBeenCalledTimes(1);
      expect(findRemoteCity).not.toHaveBeenCalled();
      expect(filterFeaturesSpy).not.toHaveBeenCalled();
      expect(compileLocationDocumentsSpy).not.toHaveBeenCalled();
      expect(saveLocationSpy).not.toHaveBeenCalled();
      result.locations.forEach(el => {
        expect(el.score).toBeGreaterThan(1);
        expect(el).toHaveProperty('geometry.type');
        expect(el).toHaveProperty('geometry.coordinates');
        expect(el).toHaveProperty('name');
        expect(el).toHaveProperty('county');
        expect(el).toHaveProperty('displayName');
        expect(el).toHaveProperty('country');
      });
      findLocalCitySpy.mockRestore();
      filterFeaturesSpy.mockRestore();
      findRemoteCity.mockClear();
      compileLocationDocumentsSpy.mockRestore();
      saveLocationSpy.mockRestore();
    });
    test('Not located in DB - Return location with no score', async () => {
      const testLocation = mockFeaturesMerged.filter(el =>
        _.toLower(el.displayName).includes('newry')
      );
      await LocationModel.insertMany(testLocation);
      const findLocalCitySpy = jest.spyOn(LocationHandlers, 'findLocalCity');
      const filterFeaturesSpy = jest.spyOn(LocationHandlers, 'filterFeatures');
      const compileLocationDocumentsSpy = jest.spyOn(
        LocationHandlers,
        'compileLocationDocuments'
      );
      const consoleLogSpy = jest
        .spyOn(console, 'log')
        .mockImplementation(() => null);
      const saveLocationSpy = jest
        .spyOn(LocationHandlers, 'saveLocation')
        .mockImplementation(() => null);
      const result = await LocationHandlers.getCity('carrickmacross monaghan');
      expect(findLocalCitySpy).toHaveBeenCalledWith(
        'carrickmacross monaghan',
        2
      );
      expect(findRemoteCity).toHaveBeenCalledWith('carrickmacross monaghan');
      expect(findRemoteCity).toHaveBeenCalledTimes(1);
      expect(filterFeaturesSpy).toHaveBeenCalledTimes(1);
      expect(compileLocationDocumentsSpy).toHaveBeenCalledTimes(1);
      expect(consoleLogSpy).toHaveBeenNthCalledWith(
        1,
        expect.stringContaining('not found in DB, Connecting to Nominatim...')
      );
      expect(consoleLogSpy).toHaveBeenNthCalledWith(
        2,
        expect.stringContaining('found in Nominatim, compiling...')
      );
      expect(result.locations).toBeArrayOfSize(1);
      const loc = _.head(result.locations);
      expect(saveLocationSpy).toBeCalled();
      expect(loc).toHaveProperty('score', null);
      findLocalCitySpy.mockRestore();
      filterFeaturesSpy.mockRestore();
      findRemoteCity.mockClear();
      compileLocationDocumentsSpy.mockRestore();
      saveLocationSpy.mockRestore();
      consoleLogSpy.mockRestore();
    });
  });
  describe('Invalid arguments', () => {
    test('Return 404 not found and correct message', async () => {
      const testLocation = mockFeaturesMerged;
      await LocationModel.insertMany(testLocation);
      const findLocalCitySpy = jest.spyOn(LocationHandlers, 'findLocalCity');
      const filterFeaturesSpy = jest.spyOn(LocationHandlers, 'filterFeatures');
      const compileLocationDocumentsSpy = jest.spyOn(
        LocationHandlers,
        'compileLocationDocuments'
      );
      const saveLocationSpy = jest.spyOn(LocationHandlers, 'saveLocation');
      const consoleLogSpy = jest
        .spyOn(console, 'log')
        .mockImplementation(() => null);
      await expect(
        LocationHandlers.getCity('somewhere nowhere')
      ).rejects.toThrow(
        expect.objectContaining({
          message: 'Not found',
          code: 404
        })
      );
      expect(findLocalCitySpy).toHaveBeenCalledWith('somewhere nowhere', 2);
      expect(findLocalCitySpy).toHaveBeenCalledTimes(1);
      expect(findRemoteCity).toHaveBeenCalledWith('somewhere nowhere');
      expect(findRemoteCity).toHaveBeenCalledTimes(1);
      expect(consoleLogSpy).toHaveBeenNthCalledWith(
        1,
        expect.stringContaining('not found in DB, Connecting to Nominatim...')
      );
      expect(consoleLogSpy).toHaveBeenNthCalledWith(
        2,
        expect.stringContaining('not Found in Nominatim, returning 404')
      );
      expect(filterFeaturesSpy).not.toHaveBeenCalled();
      expect(compileLocationDocumentsSpy).not.toHaveBeenCalled();
      expect(saveLocationSpy).not.toHaveBeenCalled();
      findLocalCitySpy.mockRestore();
      filterFeaturesSpy.mockRestore();
      findRemoteCity.mockClear();
      compileLocationDocumentsSpy.mockRestore();
      saveLocationSpy.mockRestore();
      consoleLogSpy.mockRestore();
    });
  });
  test.todo('Include Id in locations');
});
