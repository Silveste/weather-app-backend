const GJV = require('geojson-validation');
const _ = require('lodash');
const TurfHelpers = require('@turf/helpers');
const polygonContains = require('@turf/boolean-contains').default;
const mongoose = require('mongoose');
const Helpers = require('./helpers');
const Weather = require('./weather');
const WeatherArea = require('../models/weatherArea');
const weatherAreaData = require('../models/__mocks__/weather-mock-data.json');
jest.mock('../apis/openWeather');
const apiOpenWeather = require('../apis/openWeather');

describe('Create a polygon slot from coordinates - Function getSlot', () => {
  let mockParams;
  beforeEach(() => {
    mockParams = {
      lat: 102.3,
      lon: 79.2,
      map: {
        type: 'Polygon',
        coordinates: [
          [
            [0, 0],
            [100, 100],
            [50, 150],
            [-50, 50],
            [0, 0]
          ]
        ]
      },
      slotSide: 10.0
    };
  });
  describe('Accept 3 parameters: lat, lon, map (Polygon), slotSide', () => {
    test('Thorws error if map is not polygon', () => {
      const wrongMap = {
        type: 'LineString',
        coordinates: [
          [
            [102.0, 0.0],
            [103.0, 1.0],
            [104.0, 0.0],
            [105.0, 1.0]
          ]
        ]
      };
      const { lat, lon, slotSide } = mockParams;
      expect(() => Weather.getSlot(lat, lon, wrongMap, slotSide)).toThrow();
    });
    test('Throws error if map polygon has more than one LinearRing', () => {
      const wrongMap = {
        type: 'Polygon',
        coordinates: [
          [
            [102.0, 0.0],
            [103.0, 1.0],
            [104.0, 2.0],
            [105.0, 3.0],
            [102.0, 0.0]
          ],
          [
            [102.5, 0.75],
            [103.0, 1.5],
            [104.0, 2.5],
            [104.5, 3.0],
            [102.5, 0.75]
          ]
        ]
      };
      const { lat, lon, slotSide } = mockParams;
      expect(() => Weather.getSlot(lat, lon, wrongMap, slotSide)).toThrow();
    });
    test('Throws an error if map polygon is not a rectangle', () => {
      const wrongMap = {
        type: 'Polygon',
        coordinates: [
          [
            [0, 0],
            [0, 75],
            [100, 100],
            [100, 25],
            [0, 0]
          ]
        ]
      };
      const { lat, lon, slotSide } = mockParams;
      expect(() => Weather.getSlot(lat, lon, wrongMap, slotSide)).toThrow();
    });
    test.each([
      ['is negative', -4],
      ['is zero', 0],
      ['bigger than map', 20000]
    ])('Throw error if slotSide %s', (_, wrongSlotSide) => {
      const { lat, lon, map } = mockParams;
      expect(() => Weather.getSlot(lat, lon, map, wrongSlotSide)).toThrow();
    });
    test('throws error if coordinates are out of bounds', () => {
      const lat = 45;
      const lon = 50;
      const { slotSide, map } = mockParams;
      expect(() => Weather.getSlot(lat, lon, map, slotSide)).toThrow();
    });
    test('throws error if coordinates are on the bottom border (out of bounds))', () => {
      const lat = 50;
      const lon = 50;
      const { slotSide, map } = mockParams;
      expect(() => Weather.getSlot(lat, lon, map, slotSide)).toThrow();
    });
    test('throws error if coordinates are on the right border (out of bounds))', () => {
      const lat = 125;
      const lon = 75;
      const { slotSide, map } = mockParams;
      expect(() => Weather.getSlot(lat, lon, map, slotSide)).toThrow();
    });
    test('throws error if coordinates are on the top border (out of bounds))', () => {
      const lat = 100;
      const lon = 0;
      const { slotSide, map } = mockParams;
      expect(() => Weather.getSlot(lat, lon, map, slotSide)).toThrow();
    });
    test('throws error if coordinates are on the left border (out of bounds))', () => {
      const lat = 25;
      const lon = -25;
      const { slotSide, map } = mockParams;
      expect(() => Weather.getSlot(lat, lon, map, slotSide)).toThrow();
    });
  });
  describe('Returns a Polygon representing a slot', () => {
    test('Polygon contains one LinearRing representing a rectangle', () => {
      const { lat, lon, map, slotSide } = mockParams;
      const result = Weather.getSlot(lat, lon, map, slotSide);
      expect(GJV.isPolygon(result)).toBe(true);
      expect(result.coordinates).toBeArrayOfSize(1);
      expect(Helpers.isRectangle(result.coordinates[0])).toBe(true);
    });
    test('The arguments latitude and longitude are contained in slot', () => {
      const { lat, lon, map, slotSide } = mockParams;
      const point = TurfHelpers.point([lon, lat]);
      const result = Weather.getSlot(lat, lon, map, slotSide);
      expect(polygonContains(result, point)).toBe(true);
    });
    test('Polygon sides are no longer than slotSide', () => {
      const { lat, lon, map, slotSide } = mockParams;
      const result = Weather.getSlot(lat, lon, map, slotSide);
      const rCoords = result.coordinates[0];
      const sideALength = Helpers.getVectorLength(rCoords[0], rCoords[1]);
      const sideBLength = Helpers.getVectorLength(rCoords[1], rCoords[2]);
      expect(sideALength).toBeLessThanOrEqual(slotSide);
      expect(sideBLength).toBeLessThanOrEqual(slotSide);
    });
    test('Reminder of MapSides dibvided by respective slotSides is equal to zero', () => {
      const { lat, lon, map, slotSide } = mockParams;
      const result = Weather.getSlot(lat, lon, map, slotSide);
      const mapCoord = map.coordinates[0];
      const mapSideA = Helpers.getVectorLength(mapCoord[0], mapCoord[1]);
      const mapSideB = Helpers.getVectorLength(mapCoord[1], mapCoord[2]);
      const rCoords = result.coordinates[0];
      const slotSide1 = Helpers.getVectorLength(rCoords[0], rCoords[1]);
      const slotSide2 = Helpers.getVectorLength(rCoords[1], rCoords[2]);
      const dec = 4;
      const match1 =
        _.round(mapSideA % slotSide1, dec) === 0 &&
        _.round(mapSideB % slotSide2, dec) === 0;
      const match2 =
        _.round(mapSideA % slotSide2, dec) === 0 &&
        _.round(mapSideB % slotSide1, dec) === 0;
      expect(match1 || match2).toBe(true);
    });
    test('Slot sides length is the closest possible to max slot side length', () => {
      const { lat, lon, map, slotSide } = mockParams;
      const result = Weather.getSlot(lat, lon, map, slotSide);
      const mapCoord = map.coordinates[0];
      const mapSideA = Helpers.getVectorLength(mapCoord[0], mapCoord[1]);
      const mapSideB = Helpers.getVectorLength(mapCoord[1], mapCoord[2]);
      const maxSideALength = mapSideA / Math.ceil(mapSideA / slotSide);
      const maxSideBLength = mapSideB / Math.ceil(mapSideB / slotSide);
      const rCoords = result.coordinates[0];
      const slotSide1 = Helpers.getVectorLength(rCoords[0], rCoords[1]);
      const slotSide2 = Helpers.getVectorLength(rCoords[1], rCoords[2]);
      const dec = 4;
      const match1 =
        _.round(maxSideALength / slotSide1, dec) === 1 &&
        _.round(maxSideBLength / slotSide2, dec) === 1;
      const match2 =
        _.round(maxSideALength / slotSide2, dec) === 1 &&
        _.round(maxSideBLength / slotSide1, dec) === 1;
      expect(match1 || match2).toBe(true);
    });
  });
});
describe('Find local weather area - function findLocalWeatherArea', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      {
        bufferCommands: false,
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
      },
      err => {
        if (err) throw new Error(err);
      }
    );
  });
  beforeEach(async () => {
    await WeatherArea.deleteMany();
    await WeatherArea.create(weatherAreaData);
    await WeatherArea.syncIndexes();
  });
  afterAll(async () => {
    await mongoose.disconnect();
  });
  test.each([
    [
      {
        type: 'Point',
        coordinates: [
          [
            [0, 0],
            [100, 0],
            [100, 50],
            [0, 50],
            [0, 0]
          ]
        ]
      }
    ],
    [
      {
        type: 'Polygon',
        coordinates: [
          [
            [0, 0],
            [100, 0],
            [100, 50],
            [0, 50],
            [0, 0]
          ],
          [
            [20, 20],
            [80, 20],
            [80, 40],
            [20, 40],
            [20, 20]
          ]
        ]
      }
    ],
    [
      {
        type: 'Polygon',
        coordinates: [
          [
            [0, 0],
            [100, 0],
            [100, 50],
            [0, 50],
            [20, 0]
          ]
        ]
      }
    ],
    [
      {
        type: 'Polygon',
        coordinates: [
          [
            [0, 0],
            [100, 0],
            [100, 50],
            [0, 50]
          ]
        ]
      }
    ]
  ])(
    '%# - Throws error if parameter is GeoJson Polygon (rectangle with one LinearRing)',
    async slot => {
      await expect(Weather.findLocalWeatherArea(slot)).rejects.toThrow();
    }
  );
  test('Returns a promise that resolves to a weather area document if found', async () => {
    const slot = weatherAreaData.grid;
    const result = await Weather.findLocalWeatherArea(slot);
    expect(result).toHaveProperty('_id');
    expect(result.grid).toBe(slot);
    const validationResult = await WeatherArea.validate(result);
    expect(validationResult).toBeUndefined();
  });
  test('Returns null if not found', async () => {
    const slot = '0.00,0.00,|10.00,0.00|10.00,-10.00|0.00,-10.00';
    const result = await Weather.findLocalWeatherArea(slot);
    expect(result).toBeNull();
  });
  test('Throws error if mongoose is not ready', async () => {
    await mongoose.disconnect();
    const slot = weatherAreaData.geometry;
    await expect(Weather.findLocalWeatherArea(slot)).rejects.toThrow();
  });
});
describe('Save a weather Area - function saveWeatherArea', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      {
        bufferCommands: false,
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
      },
      err => {
        if (err) throw new Error(err);
      }
    );
  });
  beforeEach(async () => {
    await WeatherArea.deleteMany();
    // await WeatherArea.collection.insertOne(weatherAreaData);
    await WeatherArea.syncIndexes();
  });
  afterAll(async () => {
    await mongoose.disconnect();
  });
  describe('Save successfully - return promise that resolves to true', () => {
    test('WeatherArea is saved in the DB', async () => {
      const saved = await Weather.saveWeatherArea(weatherAreaData);
      expect(saved instanceof mongoose.Document).toBe(true);
      expect(saved).toHaveProperty('_id');
      expect(saved).toHaveProperty('updatedAt');
      expect(saved.updatedAt).toEqual(expect.any(Date));
      expect(saved.toObject().grid).toBe(weatherAreaData.grid);
    });
    test('If WA with same coordinates is in DB override it', async () => {
      const otherWeatherAreaData = {
        ...weatherAreaData,
        list: [
          {
            ...weatherAreaData.list[0],
            dt: 2591963200000
          }
        ]
      };
      await Weather.saveWeatherArea(weatherAreaData);
      const saved1 = await WeatherArea.find().exec();
      await Weather.saveWeatherArea(otherWeatherAreaData);
      const saved2 = await WeatherArea.find().exec();
      expect(saved1.length === 1).toBe(true);
      expect(saved2.length === 1).toBe(true);
      expect(saved2[0]._id).toStrictEqual(saved1[0]._id);
      expect(saved2[0].list[0].dt.getTime()).toBe(
        otherWeatherAreaData.list[0].dt
      );
      expect(saved2[0].updatedAt.getTime()).toBeGreaterThan(
        saved1[0].updatedAt.getTime()
      );
    });
    test('DB query error, log the error return false', async () => {
      await mongoose.disconnect();
      jest.spyOn(console, 'error').mockImplementation(() => {});
      try {
        const result = await Weather.saveWeatherArea(weatherAreaData);
        expect(result).toBeNil();
        expect(console.error).toHaveBeenCalledTimes(1);
      } finally {
        console.error.mockRestore();
        await mongoose.connect(
          global.__MONGO_URI__,
          {
            bufferCommands: false,
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
          },
          err => {
            if (err) throw new Error(err);
          }
        );
      }
    });
    test.each([
      [
        {
          ...weatherAreaData,
          grid: '0,0|10,0|10.00,10.00|0.00,10.00'
        }
      ],
      [
        {
          ...weatherAreaData,
          list: [{ ...weatherAreaData.list[0], dt: 'aaaa' }]
        }
      ]
    ])(
      '%# - If not valid weatherArea log error, return null',
      async invalidWeatherArea => {
        jest.spyOn(console, 'error').mockImplementation(() => {});
        try {
          const result = await Weather.saveWeatherArea(invalidWeatherArea);
          expect(result).toBeNil();
          expect(console.error).toHaveBeenCalledTimes(1);
        } finally {
          console.error.mockRestore();
        }
      }
    );
  });
});
describe('Get a weather area in function of an area - function getWeatherArea', () => {
  //Mock Parameters, arguments and responses
  const { mapEdges, maxSlotSideLength } = require('../config.json');
  const { north, south, west, east } = _.mapValues(mapEdges, val =>
    Number(val)
  );
  const slotLength = Number(maxSlotSideLength);
  const mapGeometry = {
    type: 'Polygon',
    coordinates: [
      [
        [west, south],
        [east, south],
        [east, north],
        [west, north],
        [west, south]
      ]
    ]
  };
  // NOTE areas array: Both areas are equivalent, getWeatherArea should transform the polygon geometry (areas[1]) into the point (areas[0])
  const areas = [
    { type: 'Point', coordinates: [-9.5, 53.5] },
    {
      type: 'Polygon',
      coordinates: [
        [
          [-10, 53],
          [-9, 53],
          [-9, 54],
          [-10, 54],
          [-10, 53]
        ]
      ]
    }
  ];
  const getSlotMockResponses = [
    {
      type: 'Polygon',
      coordinates: [
        [
          [-9.6, 53.36],
          [-9.56, 53.36],
          [-9.56, 53.4],
          [-9.6, 53.4],
          [-9.6, 53.36]
        ]
      ]
    }
  ];
  const grid = '-9.60,53.36|-9.56,53.36|-9.56,53.40|-9.60,53.40';
  // NOTE slotcenter is calculated using @turf/centerOfMass
  const slotCenter = {
    type: 'Point',
    coordinates: [-9.58, 53.38]
  };
  const findLocalweatherAreaMockResponses = [
    {
      list: new Array(7).map(() => {
        const props = _.pick(weatherAreaData.list[0], [
          'temp',
          'wind',
          'main',
          'dt',
          'sunrise',
          'sunset',
          'clouds',
          'rain',
          'snow'
        ]);
        return {
          ...props,
          _id: '5f2045ddb98a62a140aa0de7'
        };
      }),
      updatedAt: new Date(),
      createdAt: new Date(),
      __v: 0,
      _id: '5f2045ddb98a62a140aa0de6',
      grid
    }
  ];
  // Define Spies
  let getSlotSpy, findLocalWeatherAreaSpy, saveWeatherAreaSpy;
  beforeAll(() => {
    getSlotSpy = jest
      .spyOn(Weather, 'getSlot')
      .mockReturnValue(getSlotMockResponses[0]);
    findLocalWeatherAreaSpy = jest
      .spyOn(Weather, 'findLocalWeatherArea')
      .mockReturnValue(Promise.resolve(findLocalweatherAreaMockResponses[0]));
    saveWeatherAreaSpy = jest
      .spyOn(Weather, 'saveWeatherArea')
      .mockReturnValue(Promise.resolve(findLocalweatherAreaMockResponses[0]));
  });

  //After each reset spies
  afterEach(() => {
    apiOpenWeather.getWeatherFromRemote.mockClear();
    getSlotSpy.mockClear();
    findLocalWeatherAreaSpy.mockClear();
    saveWeatherAreaSpy.mockClear();
  });
  afterAll(() => {
    getSlotSpy.mockRestore();
    findLocalWeatherAreaSpy.mockRestore();
    saveWeatherAreaSpy.mockRestore();
  });
  test.each([
    { type: 'Polygon', coordinates: [0, 0] },
    {
      type: 'Point',
      coordinates: [
        [
          [0, 0],
          [0, 10],
          [10, 10],
          [10, 0],
          [0, 0]
        ]
      ]
    },
    {
      type: 'Polygon',
      coordinates: [
        [
          [0, 0],
          [0, 10],
          [10, 10],
          [10, 0]
        ]
      ]
    },
    {
      type: 'Polygon',
      coordinates: [
        [
          [0, 0],
          [0, 10],
          [10, 10],
          [10, 0],
          [0, 0]
        ],
        [
          [2, 2],
          [2, 8],
          [8, 8],
          [8, 2],
          [2, 2]
        ]
      ]
    }
  ])(
    '%# Argument is a valid Polygon or Point, throws error if not valid',
    async area => {
      await expect(Weather.getWeatherArea(area)).rejects.toThrow();
    }
  );
  test('Calls getSlot with correct, point (lat,lon), map and maxSlot', async () => {
    await Weather.getWeatherArea(areas[0]);
    expect(getSlotSpy).toHaveBeenCalledTimes(1);
    expect(getSlotSpy).toHaveBeenCalledWith(
      areas[0].coordinates[1],
      areas[0].coordinates[0],
      mapGeometry,
      slotLength
    );
  });
  test('When received polygon calls getSlot, with point in center of polygon', async () => {
    await Weather.getWeatherArea(areas[1]);
    expect(getSlotSpy).toHaveBeenCalledTimes(1);
    expect(getSlotSpy).toHaveBeenCalledWith(
      areas[0].coordinates[1],
      areas[0].coordinates[0],
      mapGeometry,
      slotLength
    );
  });
  test('Search for weatherArea locally', async () => {
    await Weather.getWeatherArea(areas[0]);
    expect(findLocalWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(findLocalWeatherAreaSpy).toHaveBeenCalledWith(grid);
  });
  test('Local weatherArea present and updated, do not call remote API, do not update it', async () => {
    await Weather.getWeatherArea(areas[0]);
    expect(findLocalWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(saveWeatherAreaSpy).toHaveBeenCalledTimes(0);
    expect(apiOpenWeather.getWeatherFromRemote).toHaveBeenCalledTimes(0);
  });
  test('Fail to get local weather area, log error, call remote API and update to DB', async () => {
    const consoleSpy = jest
      .spyOn(console, 'error')
      .mockImplementation(() => {});
    findLocalWeatherAreaSpy.mockReturnValueOnce(
      Promise.reject(new Error('Error from test'))
    );
    await Weather.getWeatherArea(areas[0]);
    expect(findLocalWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(saveWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(apiOpenWeather.getWeatherFromRemote).toHaveBeenCalledTimes(1);
    expect(consoleSpy).toHaveBeenCalledTimes(1);
    consoleSpy.mockRestore();
  });
  test('Local weatherArea not present, call remote API and save to DB', async () => {
    findLocalWeatherAreaSpy.mockReturnValueOnce(Promise.resolve(null));
    await Weather.getWeatherArea(areas[0]);
    expect(findLocalWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(saveWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(apiOpenWeather.getWeatherFromRemote).toHaveBeenCalledTimes(1);
  });
  test('Local weatherArea not updated, call remote API and update to DB', async () => {
    findLocalWeatherAreaSpy.mockReturnValueOnce(
      Promise.resolve({
        ...findLocalweatherAreaMockResponses[0],
        updatedAt: new Date(Date.now() - 24 * 60 * 60 * 1000)
      })
    );
    await Weather.getWeatherArea(areas[0]);
    expect(findLocalWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(saveWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(apiOpenWeather.getWeatherFromRemote).toHaveBeenCalledTimes(1);
  });
  test('Call to remote API using the center of slot coordinates', async () => {
    findLocalWeatherAreaSpy.mockReturnValueOnce(Promise.resolve(null));
    await Weather.getWeatherArea(areas[0]);
    expect(apiOpenWeather.getWeatherFromRemote).toHaveBeenCalledTimes(1);
    expect(apiOpenWeather.getWeatherFromRemote).toHaveBeenCalledWith(
      ...slotCenter.coordinates.reverse()
    );
  });
  test('Call to seveWeatherArea with object from getSlot and apiOpenWeather responses', async () => {
    findLocalWeatherAreaSpy.mockReturnValueOnce(Promise.resolve(null));
    await Weather.getWeatherArea(areas[0]);
    expect(saveWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(saveWeatherAreaSpy).toHaveBeenCalledWith({
      grid,
      list: await apiOpenWeather.getWeatherFromRemote.mock.results[0].value
    });
  });
  test('Return weatherArea (from local) with correct properties', async () => {
    const result = await Weather.getWeatherArea(areas[0]);
    expect(result).toHaveProperty('geometry');
    expect(result).toHaveProperty('list');
    expect(result).not.toHaveProperty('__v');
    expect(result).not.toHaveProperty('_id');
    expect(result).not.toHaveProperty('createdAt');
    expect(result).not.toHaveProperty('updatedAt');
    expect(result).not.toHaveProperty(['geometry', '_id']);
    expect(result.list).toBeArrayOfSize(7);
    result.list.forEach(item => expect(item).not.toHaveProperty('_id'));
  });
  test('Return weatherArea (from remote) with correct properties', async () => {
    findLocalWeatherAreaSpy.mockReturnValueOnce(Promise.resolve(null));
    const result = await Weather.getWeatherArea(areas[0]);
    expect(result).toHaveProperty('geometry');
    expect(result).toHaveProperty('list');
    expect(result).not.toHaveProperty('__v');
    expect(result).not.toHaveProperty('_id');
    expect(result).not.toHaveProperty('createdAt');
    expect(result).not.toHaveProperty('updatedAt');
    expect(result).not.toHaveProperty(['geometry', '_id']);
    expect(result.list).toBeArrayOfSize(7);
    result.list.forEach(item => expect(item).not.toHaveProperty('_id'));
  });
  test('Cannot get remote weatherArea and local is not updated, return not updated and log error', async () => {
    findLocalWeatherAreaSpy.mockReturnValueOnce(
      Promise.resolve({
        ...findLocalweatherAreaMockResponses[0],
        updatedAt: new Date(Date.now() - 24 * 60 * 60 * 1000)
      })
    );
    apiOpenWeather.getWeatherFromRemote.mockRejectedValue(
      new Error('Error from test')
    );
    const consoleSpy = jest
      .spyOn(console, 'error')
      .mockImplementation(() => {});
    const result = await Weather.getWeatherArea(areas[0]);
    expect(apiOpenWeather.getWeatherFromRemote).toHaveBeenCalledTimes(1);
    expect(findLocalWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(saveWeatherAreaSpy).toHaveBeenCalledTimes(0);
    expect(consoleSpy).toHaveBeenCalledTimes(1);
    expect(result).toHaveProperty('geometry');
    expect(result).toHaveProperty('list');
    expect(result).toHaveProperty('id');
    expect(result).not.toHaveProperty('_id');
    expect(result).not.toHaveProperty('__v');
    expect(result).not.toHaveProperty('createdAt');
    expect(result).not.toHaveProperty('updatedAt');
    expect(result).not.toHaveProperty(['geometry', '_id']);
    expect(result.list).toBeArrayOfSize(7);
    result.list.forEach(item => expect(item).not.toHaveProperty('_id'));
    consoleSpy.mockRestore();
  });
  test('Cannot get remote weatherArea and no local, throw an error', async () => {
    findLocalWeatherAreaSpy.mockReturnValueOnce(Promise.resolve(null));
    apiOpenWeather.getWeatherFromRemote.mockRejectedValue(
      new Error('Error from test')
    );
    const consoleSpy = jest
      .spyOn(console, 'error')
      .mockImplementation(() => {});
    await expect(Weather.getWeatherArea(areas[0])).rejects.toThrow();
    expect(apiOpenWeather.getWeatherFromRemote).toHaveBeenCalledTimes(1);
    expect(findLocalWeatherAreaSpy).toHaveBeenCalledTimes(1);
    expect(saveWeatherAreaSpy).toHaveBeenCalledTimes(0);
    expect(consoleSpy).toHaveBeenCalledTimes(1);
    consoleSpy.mockRestore();
  });
});
/*
WEATHER END POINT WITH COORDINATES => Returns weather area
WEATHER END POINT WITH QUERY => Returns weather area (Express end point will manage this)

END POINT WITH COORDINATES
1- Need to validate weather coords => Helpers.valCoords (Express end point will manage this)
2- Need to validate if is between map bounds => Helpers.isInMAp (Express end point will manage this)



END POINT WITH QUERY
1- Need to decode query
2- Get location using query,
3- if null return 404 error
4- continue point 3 of END POINT WITH COORDINATES
*/
