const polygonContains = require('@turf/boolean-contains').default;
const GJV = require('geojson-validation');
const _ = require('lodash');
const { mapEdges } = require('../config.json');

module.exports = class {
  /*
  Validates coordinate
  @requires: --
  @params:
    coord: coordinate float to validate
    decimalDigits: Indicates the number of digits for the decimals of the coordinate
  @returns: float with coordinate rounded to 4 decimals
  */
  static valCoord(coord, decimalDigits = 4) {
    // Valid coord only can contain "+"" or "-" and "XX.XXXXXX" where X are numbers
    if (!coord.toString().match(/^[+-]?[0-9]{1,2}(\.[0-9]+)?$/)) {
      return NaN;
    }
    const number = parseFloat(coord);
    const mult = 10 ** decimalDigits;
    return Math.round(number * mult) / parseFloat(mult);
  }

  /*
  Check if point is inside map
  @requires: mapEdges, polygonContains
  @params:
    lat: latitude to validate
    lon: longitude to validate
  @returns: boolean
  */
  static isInMap(geometry) {
    if (geometry.type !== 'Point' && geometry.type !== 'Polygon') {
      throw new Error(
        `isInMap only accepts geometries type Polygon or Point, received: ${geometry.type}`
      );
    }
    const north = parseFloat(mapEdges.north);
    const south = parseFloat(mapEdges.south);
    const west = parseFloat(mapEdges.west);
    const east = parseFloat(mapEdges.east);
    const mapPolygon = {
      type: 'Polygon',
      coordinates: [
        [
          [west, north],
          [east, north],
          [east, south],
          [west, south],
          [west, north]
        ]
      ]
    };
    return polygonContains(mapPolygon, geometry);
  }

  /*
  Decodes URL encoded string
  @requires: --
  @params:
    uriComponent: string to decode
  @returns: string decoded
  */
  static decoder(uriComponent) {
    return decodeURIComponent(uriComponent.replace(/\+/g, ' '));
  }

  /*
  Returns true id element passed is a Linearring (selfclosing geojson Linear string) that describes a rectangle
  @requires: --
  @params:
    linearRing: Array of arrays describing points (linearstring coordinate)
  @returns: boolean
  */
  static isRectangle(linearRing) {
    // Throw error if object is not a valid line string coordinate
    if (!GJV.isLineStringCoor(linearRing)) {
      throw new Error(
        'Argument must be a valid linear string coordinates array'
      );
    }
    // Throw error if line string is unclosed
    const head = _.head(linearRing);
    const last = _.last(linearRing);
    if (!_.isEqual(head, last)) {
      throw new Error('Argument must be a closed line string');
    }
    // Linestring must have only 5 points (extremes describe the same point)
    if (linearRing.length !== 5) return false;
    // Get sides length
    const sideA = this.getVectorLength(linearRing[0], linearRing[1]);
    const sideB = this.getVectorLength(linearRing[1], linearRing[2]);
    const sideC = this.getVectorLength(linearRing[2], linearRing[3]);
    const sideD = this.getVectorLength(linearRing[3], linearRing[4]);
    // Get diagonals length
    const diag1 = this.getVectorLength(linearRing[0], linearRing[2]);
    const diag2 = this.getVectorLength(linearRing[1], linearRing[3]);
    //polygon is a rectangle if oposite sides and diagonals are equal
    let result = true;
    result = sideA === sideC && result;
    result = sideB === sideD && result;
    result = diag1 === diag2 && result;
    return result;
  }

  /**
   * [
   * Returns true if the arg passed is a valid grid index string.
   * A valid grid index is a string of 4 unique points separated by "sep"
   * parameter where each point is defined by 2 float numbers separated
   * by a comma ","
   * Each number has a fixed number of decimals indicated by
   * "prec" paramater
   * ]
   * @requires {this.isRectangle}
   * @param {String} gridIdx [Value to be validated]
   * @param {Character} sep [Separator between points. I cannot be a comma "," a point ".", dash "-" or a number. Default to "|"]
   * @param {Number} prec [Precision of the X and Y coordinates. Indicates the number of decimals. If number has less decimals it will be filled by Zeros. Values: (0-5], default : 2]
   * @return {Boolean} [true if is valid]
   */
  static isGridIndex(gridIdx, sep = '|', prec = 2) {
    //Validating sep
    const invalidSepChars = ',.-0123456789';
    if (sep.length !== 1 || invalidSepChars.includes(sep)) {
      throw new Error('Invalid separator');
    }

    //Validating prec
    if (isNaN(prec)) {
      throw new Error(`prec is Nan, value: ${prec}`);
    }
    if (!prec || prec <= 0 || prec > 5) {
      throw new Error(`Invalid prec value: ${prec}`);
    }

    //Regex validation
    //Escaping separator
    const escapedSep = sep.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

    //Coordinate: (-)d(one or more).d(prec times)
    const numRegexStr = `-?\\d+\\.\\d{${prec}}`;

    //Point: coordinate,coordinate
    const pointRegexStr = `${numRegexStr},${numRegexStr}`;

    // gridIdx: point sep point sep point sep point
    const gridIdxRegexStr = new Array(4).fill(pointRegexStr).join(escapedSep);
    const regex = new RegExp(`^${gridIdxRegexStr}$`);

    if (!regex.test(gridIdx)) return false;

    //Rectangle validation
    const points = gridIdx.split(sep);
    const pointsNumbers = points.map(pt =>
      pt.split(',').map(c => Number.parseFloat(c))
    );
    const linearRing = [...pointsNumbers, pointsNumbers[0]];
    return this.isRectangle(linearRing);
  }

  /**
   * [Returns true if the argument is a valid grid geojson geometry
   * string.
   * A valid geometry is a Polygon with one LinearRing with 5 postions
   * representing a rectangle
   * For more information see: https://geojson.org]
   * @requires {this.isRectangle}
   * @param {GEOJson geometry} [Value to be validated]
   * @return {Boolean} [true if is valid]
   */
  static isGridGeometry(geometry) {
    const hasOneLinerarRing = geometry.coordinates.length === 1;
    const linearRing = geometry.coordinates[0];
    if (
      hasOneLinerarRing &&
      GJV.isPolygon(geometry) &&
      this.isRectangle(linearRing)
    ) {
      return true;
    }
    return false;
  }

  /**
   * [Returns a valid grid index string from a valid grid GEOJson geometry]
   * @requires {this.isGridGeometry}
   * @param  {GEOJson geometry} geometry [geometry to be transformed in grid index string]
   * @param {Character} sep [Separator between points. It cannot be a comma "," a point ".", dash "-" or a number. Default to "|"]
   * @param {Number} prec [Precision of the X and Y coordinates. Indicates the number of decimals. If number has less decimals it will be filled by Zeros. Values: (0-5], default : 2]
   * @return {String} [A valid grid index string]
   */
  static getGridIndex(geometry, sep = '|', prec = 2) {
    //Validating sep
    const invalidSepChars = ',.-0123456789';
    if (!sep || sep.length !== 1 || invalidSepChars.includes(sep)) {
      throw new Error('Invalid separator');
    }

    //Validating prec
    if (isNaN(prec)) {
      throw new Error(`prec is Nan, value: ${prec}`);
    }
    if (!prec || prec <= 0 || prec > 5) {
      throw new Error(`Invalid prec value: ${prec}`);
    }

    //Validate geometry
    if (!this.isGridGeometry(geometry)) {
      throw new Error('Invalid geometry');
    }
    const points = geometry.coordinates[0].slice(0, -1);
    return points
      .map(point => point.map(c => Number(c).toFixed(prec)).join(','))
      .join(sep);
  }

  /**
   * [Returns a valid grid GEOJson geometry from a valid grid index string]
   * @requires {this.isGridIndex}
   * @param  {String} gridIdx [A valid grid index string to be transformed geometry]
   * @param {Character} sep [Separator between points. It cannot be a comma "," a point ".", dash "-" or a number. Default to "|"]
   * @param {Number} prec [Precision of the X and Y coordinates. Indicates the number of decimals. If number has less decimals it will be filled by Zeros. Values: (0-5], default : 2]
   * @return {GEOJson geometry} [Geometry representing the grid index string]
   */
  static getGridGeometry(gridIdx, sep = '|', prec = 2) {
    //Validating gridIndex
    if (!this.isGridIndex(gridIdx, sep, prec)) {
      throw new Error('Invalid arguments');
    }

    const points = gridIdx
      .split(sep)
      .map(pt => pt.split(',').map(coord => Number.parseFloat(coord)));
    const coordinates = [[...points, points[0]]];
    return { type: 'Polygon', coordinates };
  }

  /*
  Returns the length of a vector
  @requires: --
  @params:
    start: Array with X and Y components of vector origin [X, Y]
    end: Array with X and Y components of vector end [X, Y]
  @returns: float
  */
  static getVectorLength(start, end) {
    // Calculate length using euclidean norm
    const x2 = (end[0] - start[0]) ** 2;
    const y2 = (end[1] - start[1]) ** 2;
    return Math.sqrt(x2 + y2);
  }

  /*
  Returns the slope of a vector in radians from -PI/2 (not inclusive) to PI/2 (inclusive) where 0 means there is no slope in relation with X axis
  @requires: --
  @params:
    start: Array with X and Y components of vector origin [X, Y]
    end: Array with X and Y components of vector end [X, Y]
  @returns: float
  */
  static getVectorSlope(start, end) {
    const deltaY = end[1] - start[1];
    const deltaX = end[0] - start[0];
    if (deltaX === 0 && deltaY === 0) {
      throw new Error('Can\'t get the slope of zero length vector');
    } else if (deltaX === 0) {
      return Math.PI / 2;
    } else if (deltaY === 0) {
      return 0;
    }
    return Math.atan(deltaY / deltaX);
  }

  /*
  Closure that changes the origin and axis inclination of a cartesian coordinate system (CS) allowing to create a virtual coordinate system (VCS) to simplify calculations
  It provides 2 functions getOriginal and getVirtual which accept a point (Array of [x,y] coordinates) and returns the original or virtual point respectively
  @requires: lodash, getVectorLength
  @params:
    origin: Array of [X,Y] points where the origin of VCS is situated in the original CS
    slopeX: Angle in radians between X axis of VCS and X axis of original CS. It accepts values between PI/2 and -PI/2 (both not inclusive) Default = 0
    Precision: Set the decimal digits of the X Y values for the returning array (of getOriginal and getVirtual). Default is 6 and the reason of this parameters is due to the imprecise nature of JS calculations when dealing with floats.
  @returns: Object with 2 functions:
    getVirtual: Returns a point in the VCS that is equivalent to the point passed (point from original CS)
      @params:
        point: Array of [X,Y] representing a point in the original CS
      @returns:
        point: Array of [X,Y] representing a point in the VCS
    getOriginal: Returns a point in the original CS that is equivalent to the point passed (point from VCS)
      @params:
        point: Array of [X,Y] representing a point in the VCS
      @returns:
        point: Array of [X,Y] representing a point in the original CS
  */
  static createVirtualCoordinates(origin, slopeX = 0, precision = 6) {
    // Method to validate points
    const validatePoint = point => {
      if (
        !_.isArray(point) ||
        point.length !== 2 ||
        !_.isFinite(point[0]) ||
        !_.isFinite(point[1])
      ) {
        throw new Error('Invalid origin argument');
      }
    };

    const getDifference = (start, end) => {
      return end.map((p, i) => p - start[i]);
    };

    const turnPoint = (point, xAxisSlope) => {
      const module = this.getVectorLength([0, 0], point);
      if (module === 0) return [0, 0];
      const pointSlope = this.getVectorSlope([0, 0], point);
      // Get point angle
      let pointAngle;
      // quadrant 4
      if (point[0] < 0 && point[1] >= 0) {
        pointAngle = Math.PI + pointSlope; //NOTE: Slope is always negative in this quadrant
      }
      // quadrant 3
      else if (point[0] <= 0 && point[1] < 0) {
        pointAngle = Math.PI + pointSlope; //NOTE: Slope is always positive
      }
      // quadrant 2
      else if (point[0] < 0 && point[1] >= 0) {
        pointAngle = 2 * Math.PI + pointSlope; //NOTE: Slope is always negative
      }
      // quadrant 1
      else {
        pointAngle = pointSlope;
      }
      const newPointAngle = pointAngle - xAxisSlope;
      return [
        Number((module * Math.cos(newPointAngle)).toFixed(precision)),
        Number((module * Math.sin(newPointAngle)).toFixed(precision))
      ];
    };

    //Validate arguments
    validatePoint(origin);
    if (
      !_.isFinite(slopeX) ||
      slopeX >= Math.PI / 2 ||
      slopeX <= -Math.PI / 2
    ) {
      throw new Error('Invalid slope argument');
    }

    // Return functions
    return {
      getOriginal: point => {
        validatePoint(point);
        let pointCorrectSlope = point;
        if (slopeX !== 0) {
          pointCorrectSlope = turnPoint(point, slopeX * -1);
        }
        const reverseOrigin = origin.map(p => p * -1);
        return getDifference(reverseOrigin, pointCorrectSlope);
      },
      getVirtual: point => {
        validatePoint(point);
        const pointNoSlope = getDifference(origin, point);
        if (slopeX === 0) return pointNoSlope;
        return turnPoint(pointNoSlope, slopeX);
      }
    };
  }

  /*
  Returns a Date object with today's date at 0h:00m:00s
  @requires: --
  @params: --
  @returns: Date
  */
  static getToday() {
    const d = new Date();
    const dayTimeZero = d.setUTCHours(0, 0, 0, 0);
    return new Date(dayTimeZero);
  }
};
