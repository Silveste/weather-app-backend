const _ = require('lodash');
const mongoose = require('mongoose');
const area = require('@turf/area').default;
const polygonContains = require('@turf/boolean-contains').default;
const { featureTypes } = require('../config.json');
const { findRemoteCity, findRemoteCoords } = require('../apis/nominatim');
const { valCoord, isInMap, decoder } = require('./helpers');
const Location = require('../models/location');

module.exports = class {
  /*
  Find location in database from query
  @requires: Location moongose model (Must be connected to DB)
  @params:
    query: string with search terms
    minScore: float minimun search score to include found Location
  @returns: Array of location objects with score
  */
  static async findLocalCity(query, minScore) {
    const scorePath = { score: { $meta: 'textScore' } };
    if (!mongoose.connection.readyState)
      throw new Error(
        `Incorrect mongoose ready state: ${mongoose.connection.readyState}`
      );
    return Location.find({ $text: { $search: query } }, scorePath)
      .sort(scorePath)
      .then(found => {
        const result = found.reduce((acc, locationDocument) => {
          const loc = locationDocument.toObject();
          if (loc.score >= minScore) {
            acc.push({
              ...loc,
              score: loc.score / minScore
            });
          }
          return acc;
        }, []);
        if (result.length === 0) {
          throw new Error('Not found in database');
        }
        return result;
      });
  }

  /*
  Find location in database from coordinates
  @requires:
    Location moongose model (Must be connected to DB)
    area from @turf/area
  @params:
    lat: float with latitude
    lon: float with longitude
    uniqueMatch: boolean, if true function will only return the smallests marching location
    coordsPath: string, name of the paths where GeoJSON is stored
  @returns: Array of location objects
  */
  static async findLocalCoords(
    lat,
    lon,
    uniqueMatch = false,
    coordsPath = 'geometry'
  ) {
    if (!mongoose.connection.readyState)
      throw new Error(
        `Incorrect mongoose ready state: ${mongoose.connection.readyState}`
      );
    return Location.find()
      .where(coordsPath)
      .intersects()
      .geometry({
        type: 'Point',
        coordinates: [lon, lat]
      })
      .then(found => {
        if (found.length === 0) {
          throw new Error('Not found in database');
        }
        const result = [];
        if (uniqueMatch && found.length > 1) {
          const smallestDoc = found.reduce((acc, val) => {
            const accArea = area(acc.geometry);
            const valArea = area(val.geometry);
            return accArea < valArea ? acc : val;
          });
          result.push(smallestDoc.toObject());
        } else {
          result.push(...found.map(el => el.toObject()));
        }
        return result;
      });
  }

  /*
  Filter features in function of type (featuretypes enum) and return separated into points and polygons
  @requires: featureTypes, isInMap
  @params:
   featureCollection: Array of geojson features.
  @returns: Onject with 2 keys
    polygons: features with geometry type polygon
    points: Features with geometry type point
  */
  // TODO Filter out features with same characteristics, name description and county and country
  static filterFeatures(featureCollection) {
    return featureCollection.reduce(
      (acc, feature) => {
        if (!featureTypes.includes(feature.properties.type)) return acc;
        const { polygons, points } = acc;
        const geometry = feature.geometry.type.toLowerCase();
        if (geometry === 'point') {
          if (!isInMap(feature.geometry)) return acc;
          return {
            polygons,
            points: [...points, feature]
          };
        } else if (geometry === 'polygon') {
          if (!isInMap(feature.geometry)) return acc;
          return {
            polygons: [...polygons, feature],
            points
          };
        } else {
          // NOTE: Only Points and Polygons are stored. However, other geometries are logged to evaluate if is worth ot include them
          console.warn(
            `Incorrect geometry: "${feature.geometry.type}" geometry. Feature discarded\n ${feature.properties}`
          );
          return acc;
        }
      },
      { polygons: [], points: [] }
    );
  }

  /*
  @requires: findCords, _.remove, polygonContains
  @params:
   polygons: Array of features with polygon geometry.
   points: Array of features with point geometry
  @returns: Array with valid location documents
  */
  //TODO remove words that have no meaning i.e. in county remove word county
  static async compileLocationDocuments(polygonFeatures, pointFeatures) {
    // Create array for no matched polygons, initialize with all polygons
    const notMatchedPolygons = _.isNil(polygonFeatures)
      ? []
      : _.concat(polygonFeatures);
    // Map documents from points array
    const mapWithPromises = pointFeatures.map(async pointFeature => {
      try {
        // Map from Nominatim object to Location Object
        const locationPoint = this._mapToLocation(pointFeature);
        const { geometry } = locationPoint;
        // check if locationPoint is contained in any polygonFeature. If true remove it from nonMatchedPolygons
        const polygonFeatures = _.remove(notMatchedPolygons, polygonF => {
          if (polygonF.geometry) {
            return polygonContains(polygonF.geometry, geometry);
          } else {
            return false;
          }
        }).map(pF => this._mapToLocation(pF));

        // Only one polygonFeature should match the pointFeature
        // if more than one polygon has been returned log an error
        if (polygonFeatures.length > 1) {
          const ids = polygonFeatures.map(val => val.osmId).join(', ');
          // TODO sorted and choose the smaller one
          console.error(
            `${ids} polygons matched the point ${locationPoint.osmId}. Only the first one has been chosen`
          );
        }
        let polygonFeature = _.head(polygonFeatures);
        // If polygon not found, search in DB/API
        if (_.isUndefined(polygonFeature)) {
          const locations = await this.findCoords(
            geometry.coordinates[1],
            geometry.coordinates[0],
            true
          );
          polygonFeature = _.head(locations);
        }

        // Update new properties from polygon
        const newGeometry = polygonFeature.geometry;
        const polygonOsmId = polygonFeature.osmId;
        const polygonOsmType = polygonFeature.osmType;
        const polygonName = polygonFeature.name;

        //Return location with new geometry and polygon information
        return {
          ...locationPoint,
          polygonOsmId,
          polygonOsmType,
          polygonName,
          geometry: newGeometry
        };
      } catch (error) {
        console.error(
          'Cannot convert Nominatim object into Location:\n',
          error
        );
      }
    });

    const locationDocuments = await Promise.all(mapWithPromises);

    // Map not matched polygons to location object
    const locationPolygons = notMatchedPolygons.map(pF => {
      try {
        const locationPolygon = this._mapToLocation(pF);
        return locationPolygon;
      } catch (error) {
        console.error(
          'Cannot convert Nominatim object into Location:\n',
          error
        );
      }
    });
    return locationDocuments.concat(locationPolygons);
  }

  /*
  @params:
   remoteFeature: Features from Nominatim.
  @returns: Location document
  */
  static _mapToLocation(remoteFeature) {
    const { properties, geometry } = remoteFeature;
    // Build Location object
    const location = {
      type: properties.type,
      name:
        properties.city ||
        (properties.address ? properties.address.city : null) ||
        properties.name ||
        (properties.namedetails ? properties.namedetails['name:en'] : null) ||
        (properties.namedetails ? properties.namedetails['name'] : null),
      county:
        properties.county ||
        (properties.address ? properties.address.county : null),
      country:
        properties.country ||
        (properties.address ? properties.address.country : null),
      displayName: properties.display_name || properties.label,
      importance: properties.importance,
      osmId: properties.osm_id,
      osmType: properties.osm_type,
      geometry
    };
    return location;
  }

  /*
  @requires: Location mongoose model, lodash as _
  @params: location (valid Location object)
  @returns: false if not saved and true if saved
  */
  static async saveLocation(loc) {
    const { osmId, osmType, polygonOsmId, polygonOsmType } = loc;
    // Create location instance
    const location = new Location(loc);
    // Build query to check duplicates in function of polygonOsmId and polygonOsmType existence
    const searchFor = [
      { osmId, osmType },
      { polygonOsmId: osmId, polygonOsmType: osmType }
    ];
    if (_.isNumber(polygonOsmId) && _.isString(polygonOsmType)) {
      searchFor.push(
        { polygonOsmId, polygonOsmType },
        { osmId: polygonOsmId, osmType: polygonOsmType }
      );
    }
    try {
      await location.validate();
      //Check for duplicates
      const found = _.head(await Location.find({ $or: searchFor }).exec());
      if (_.isObject(found)) {
        // If origin of polygon is stored as location, remove it,
        if (found.osmId === polygonOsmId && found.osmType === polygonOsmType) {
          await Location.findByIdAndRemove(found._id).exec();
        }
        // If location already exists, log a warning and return
        else if (found.osmId === osmId && found.osmType === osmType) {
          console.warn(
            `Location osmId: ${osmId}, osmType: ${osmType} is already in DB as Location`
          );
          return false;
        }
        // If location is stored as polygon of other location, log a warning and return
        else if (
          found.polygonOsmId === osmId &&
          found.polygonOsmType === osmType
        ) {
          console.warn(
            `Location osmId: ${osmId}, osmType: ${osmType} is already in DB as geometry of ${found.osmId}, ${found.osmType}`
          );
          return false;
        }
        // Else geometry polygon is stored as polygon of other location, save which ever name matches polygon name
        else {
          console.warn(
            `Location osmId: ${osmId}, osmType: ${osmType} has the same geometry of ${found.osmId}, ${found.osmType}`
          );
          if (
            found.name !== found.polygonName &&
            loc.name === loc.polygonName
          ) {
            console.warn('Location will be replaced');
            await Location.findByIdAndRemove(found._id).exec();
          } else {
            console.warn('Location will not be replaced');
            return false;
          }
        }
      }
      await location.save();
      return true;
    } catch (e) {
      console.error('Cannot save location:\n', loc, '\nError: ', e.message);
      return false;
    }
  }

  /*
  @requires: savelocation, ompilelocationDocuments, filterFeatures, findRemoteCity, findlocalCity, decoder, lodash as _
  @params: location (valid Location object)
  @returns:
    Object { code: Number with status code, locations: Array with locations found }
    Throws an error if not found including Error.message and Error.code
  */
  static async getCity(q) {
    const query = decoder(q);
    const wordsNumber = _.words(query).length;
    let isDbAvailable = true;
    // Get locs from DB
    const locs = [];
    try {
      locs.push(...(await this.findLocalCity(query, wordsNumber)));
    } catch (e) {
      if (e.message === 'Not found in database') {
        console.log(
          `Query: "${query}" not found in DB, Connecting to Nominatim...`
        );
      } else {
        console.error('Unable Find Local City, DB not available\n', e.message);
        isDbAvailable = false;
      }
    }
    // If no locs from DB get them from API
    if (_.isEmpty(locs)) {
      const { features } = await findRemoteCity(query);
      if (!_.isEmpty(features)) {
        console.log(`Query: "${query}" found in Nominatim, compiling...`);
        const { polygons, points } = this.filterFeatures(features);
        const compiledLocs = await this.compileLocationDocuments(
          polygons,
          points
        );
        locs.push(...compiledLocs);
        // Save locs to DB
        // forEach doesn wait for promises resolution, therefore saveLocation fcn should handle any errors
        if (isDbAvailable) {
          console.log('Saving into the DB.');
          locs.forEach(loc => this.saveLocation(_.omit(loc, 'score')));
        }
      } else {
        console.log(`Query: "${query}" not Found in Nominatim, returning 404`);
        const error = new Error('Not found');
        error.code = 404;
        throw error;
      }
    }

    // Return locations with API final object
    return this._getResponseObject(locs);
  }

  /*
  @requires: valCoords, isInMap
  @params:
    lat: Latitude coordinate,
    lon: longitude coordinate
  @returns:
    Object { code: Number with status code, locations: Array with locations found }
    Throws an error if not found including Error.message and Error.code
  */
  // Todo save feaute if includes name = address.city
  static async findCoords(lat, lon, remoteOnly = false) {
    const valLat = valCoord(lat);
    const valLon = valCoord(lon);
    if (_.isNaN(valLat) || _.isNaN(valLon)) {
      const error = new Error('Invalid Coordinates');
      error.code = 400;
      throw error;
    }
    if (!isInMap({ type: 'Point', coordinates: [valLon, valLat] })) {
      const error = new Error('Not found: Coordinates out of map bounds');
      error.code = 404;
      throw error;
    }
    const locs = [];
    if (!remoteOnly) {
      try {
        locs.push(...(await this.findLocalCoords(valLat, valLon)));
      } catch (e) {
        if (e.message === 'Not found in database') {
          console.log(
            `Latitude: ${valLat} and longitude: ${valLon} not found in DB, Connecting to Nominatim...`
          );
        } else {
          console.error(
            'Unable Find Local Coords, DB not available\n',
            e.message
          );
        }
      }
    }
    if (_.isEmpty(locs)) {
      const { features } = await findRemoteCoords(valLat, valLon);
      if (_.isEmpty(features)) {
        console.log(
          `Latitude: ${valLat} and longitude: ${valLon} not Found in Nominatim, returning 404`
        );
        const error = new Error('Not found');
        error.code = 404;
        throw error;
      }
      locs.push(...features.map(feature => this._mapToLocation(feature)));
    }
    return locs;
  }

  static async getCoords(lat, lon) {
    // Find the locations
    const locs = await this.findCoords(lat, lon);
    // Return locations with API final object
    return this._getResponseObject(locs);
  }

  /*
  @requires:
  @params: locs Array with Locations objects (not mongoose objects)
  @returns:
    Object { code: Number with status code, locations: Array with locations found }
    Throws an error if not found including Error.message and Error.code
  */
  static _getResponseObject(locs) {
    // If locs is still empty throw error
    if (_.isEmpty(locs)) {
      const error = new Error('Not found');
      error.code = 404;
      throw error;
    }

    // return locs with score normalized or score = null
    const locations = locs.map(location => {
      const {
        country,
        county,
        displayName,
        name,
        type,
        geometry,
        importance,
        osmId,
        osmType
      } = location;
      return {
        id: `${osmType}-${osmId}`,
        country,
        county,
        displayName,
        name,
        type,
        importance,
        geometry: {
          type: geometry.type,
          coordinates: geometry.coordinates
        },
        score: _.isNumber(location.score) ? location.score : null
      };
    });
    return {
      code: 200,
      locations
    };
  }
};
