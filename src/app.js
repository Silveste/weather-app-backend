const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const compression = require('compression');
const _ = require('lodash');
const app = express();

const WeatherHandlers = require('./handlers/weather');
const LocationHandlers = require('./handlers/location');

// Secure the app with helmet headers
app.use(helmet());

// Set CORS for unique origin
app.use(cors({ origin: process.env.ORIGIN }));

// Set compression NOTE that only because hosting doesn provide compression
app.use(compression());

// Weather route, acept coords
app.get('/weather/coords/:lat,:lon', (req, res, next) =>
  WeatherHandlers.getWeatherArea({
    type: 'Point',
    coordinates: [Number(req.params.lon), Number(req.params.lat)]
  })
    .then(data => res.json(data))
    .catch(e => next(e.error || e))
);

// Weather route, acept query (returns only weather for first location found)
app.get('/weather/query/:q', (req, res, next) =>
  LocationHandlers.getCity(req.params.q)
    .then(data => {
      const geoJson = data.locations[0].geometry;
      return WeatherHandlers.getWeatherArea(geoJson);
    })
    .then(data => {
      return res.json(data);
    })
    .catch(e => next(e.error || e))
);

// Get location using coords
app.get('/loc/coords/:lat,:lon', (req, res, next) =>
  LocationHandlers.getCoords(req.params.lat, req.params.lon)
    .then(data => res.json(data.locations))
    .catch(e => next(e.error || e))
);

// get location using query
app.get('/loc/query/:q', (req, res, next) =>
  LocationHandlers.getCity(req.params.q)
    .then(data => res.json(data.locations))
    .catch(e => next(e.error || e))
);

//Get weather and location using query (Returns a list of locations with weather information)
app.get('/query/:q', (req, res, next) =>
  LocationHandlers.getCity(req.params.q)
    .then(data => {
      const { locations } = data;
      const promises = locations.map(loc =>
        WeatherHandlers.getWeatherArea(loc.geometry)
      );
      Promise.all(promises)
        .then(promsResolved => {
          const results = promsResolved.map((val, i) => {
            return {
              weather: val.list,
              ...locations[i]
            };
          });
          res.send(results);
        })
        .catch(e => next(e.error || e));
    })
    .catch(e => next(e.error || e))
);

//Get weather and location using coords
app.get('/coords/:lat,:lon', (req, res, next) => {
  const lat = Number(req.params.lat);
  const lon = Number(req.params.lon);
  const promiseLocation = LocationHandlers.getCoords(lat, lon);
  const promiseWeatherArea = WeatherHandlers.getWeatherArea({
    type: 'Point',
    coordinates: [lon, lat]
  });
  Promise.all([promiseWeatherArea, promiseLocation])
    .then(result => {
      res.send({
        weatherArea: result[0],
        location: result[1].locations[0]
      });
    })
    .catch(e => next(e.error || e));
});

// Invalid route handler, catch all verbs and routes if no error has been thrown
app.use((_req, _res, next) => {
  next({
    code: 400,
    message: 'Bad request'
  });
});

// Error Handler
app.use((error, req, res, next) => {
  const { message, code } = error;
  //Enforce a message
  if (!message) {
    console.error('An error without message occurs');
    res.status(500);
    res.json({
      error: {
        code: 500,
        message: 'Internal server error',
        method: req.method,
        route: req.originalUrl
      }
    });
    return;
  }

  if (!_.inRange(code, 400, 501)) {
    console.error('An internal error has occured\n', error);
    res.status(500);
    res.json({
      error: {
        code: 500,
        message: 'Internal server error',
        method: req.method,
        route: req.originalUrl
      }
    });
    return;
  }
  res.status(code);
  res.json({
    error: {
      code,
      message,
      method: req.method,
      route: req.originalUrl
    }
  });
});

module.exports = app;
