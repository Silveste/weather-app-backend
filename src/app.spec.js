const request = require('supertest');
const _ = require('lodash');
const app = require('./app');
jest.mock('helmet', () => {
  return jest.fn(() => {
    return function(req, res, next) {
      next();
    };
  });
});
const helmet = require('helmet');
jest.mock('compression', () => {
  return jest.fn(() => {
    return function(req, res, next) {
      next();
    };
  });
});
const compression = require('compression');
jest.mock('./handlers/weather');
jest.mock('./handlers/location');
const WeatherHandlers = require('./handlers/weather');
const LocationHandlers = require('./handlers/location');

describe('API ENDPOINTS', () => {
  beforeEach(() => {
    WeatherHandlers.getWeatherArea.mockClear();
    LocationHandlers.getCity.mockClear();
    LocationHandlers.getCoords.mockClear();
  });
  describe('Weather endpoint (coordinates): /weather/coords/:latitude,:longitude', () => {
    test('Status 200, Return JSON', async () => {
      const lon = -6.2603;
      const lat = 53.3498;
      return request(app)
        .get(`/weather/coords/${lat},${lon}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(WeatherHandlers.getWeatherArea).toHaveBeenCalledWith({
            type: 'Point',
            coordinates: [lon, lat]
          });
          expect(Object.keys(res.body).length).toBeGreaterThan(0);
          expect(res.body.list[0].message).toBe(
            'This object comes from a mock function'
          );
        });
    });
    test.each([
      [400, 'Invalid Area'],
      [500, 'reached openWeather limit']
    ])('Error, Response with status:  %i and message: %s', (code, message) => {
      let lon, lat;
      if (code === 400) {
        lon = -6.4;
        lat = 53.4;
      } else {
        lon = -6.5;
        lat = 53.5;
      }
      return request(app)
        .get(`/weather/coords/${lat},${lon}`)
        .expect(code)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(res.body.error).toBeDefined();
          expect(res.body.error.code).toBeDefined();
          expect(res.body.error.message).toBe(message);
        });
    });
  });
  describe('Weather endpoint (city): /weather/query/:q', () => {
    test('Status 200, Return JSON', () => {
      const query = 'Dublin';
      const lon = -6.2603;
      const lat = 53.3498;
      return request(app)
        .get(`/weather/query/${query}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(LocationHandlers.getCity).toHaveBeenCalledWith(query);
          expect(LocationHandlers.getCity).toHaveBeenCalledTimes(1);
          expect(WeatherHandlers.getWeatherArea).toHaveBeenCalledWith({
            type: 'Point',
            coordinates: [lon, lat]
          });
          expect(WeatherHandlers.getWeatherArea).toHaveBeenCalledTimes(1);
          expect(Object.keys(res.body).length).toBeGreaterThan(0);
          expect(res.body.list[0].message).toBe(
            'This object comes from a mock function'
          );
        });
    });
    test('Error, Response with status:  404 and message from getCity mock', () => {
      const query = 'Somewhere';
      return request(app)
        .get(`/weather/query/${query}`)
        .expect(404)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(res.body.error).toBeDefined();
          expect(res.body.error.code).toBeDefined();
          expect(res.body.error.message).toBe('Not found');
          expect(LocationHandlers.getCity).toHaveBeenCalledTimes(1);
          expect(WeatherHandlers.getWeatherArea).toHaveBeenCalledTimes(0);
        });
    });
    test('Error, Response with status: 500 and message from getWeatherArea mock', () => {
      const query = 'cork';
      return request(app)
        .get(`/weather/query/${query}`)
        .expect(500)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(res.body.error).toBeDefined();
          expect(res.body.error.code).toBeDefined();
          expect(res.body.error.message).toBe('reached openWeather limit');
          expect(LocationHandlers.getCity).toHaveBeenCalledTimes(1);
          expect(WeatherHandlers.getWeatherArea).toHaveBeenCalledTimes(1);
        });
    });
  });
  describe('Location endpoint (coordinates): /loc/coords/:latitude,:longitude', () => {
    test('Status 200, Expect locations array', async () => {
      const args = { lat: '53.3498', lon: '-6.2603' };
      return request(app)
        .get(`/loc/coords/${args.lat},${args.lon}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(res.body[0].city.toLowerCase()).toBe('dublin');
          expect(res.body[0].message).toBe(
            'This object comes from a mock function'
          ); // Ensure hit the mock function
          expect(res.body).not.toHaveProperty('code');
          expect(res.body).toBeArray();
          expect(LocationHandlers.getCoords).toHaveBeenCalledWith(
            args.lat,
            args.lon
          );
        });
    });
    // TODO: The following test should be tested in getCity function as parameters should be validated there
    test.each([
      ['404', 404],
      ['500', 500],
      ['555', 500],
      ['XXX', 500],
      ['null', 500]
    ])(
      'Error code: %s, returns correct error code %i and has message',
      async (errorCode, errorExpected) => {
        jest.spyOn(console, 'error').mockImplementation(() => null);
        return request(app)
          .get(`/loc/coords/${errorCode},xxxx`)
          .expect(errorExpected)
          .expect('Content-Type', /json/)
          .then(res => {
            if (_.inRange(Number(errorCode), 400, 501)) {
              expect(res.body.error.message).toBe('Error message from mock');
            } else {
              expect(res.body.error.message).not.toBe(
                'Error message from mock'
              );
            }
            expect(res.body.error.code).toBe(errorExpected);
            console.error.mockRestore();
          });
      }
    );
  });
  describe('Location endpoint (city): /loc/query/:q', () => {
    test('Status 200, JSON with latitude and longitude (200)', async () => {
      const args = { query: 'dublin' };
      return request(app)
        .get(`/loc/query/${args.query}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(res.body[0].message).toBe(
            'This object comes from a mock function'
          );
          expect(res.body).not.toHaveProperty('code');
          expect(res.body).toBeArray();
          expect(LocationHandlers.getCity).toHaveBeenCalledWith(args.query);
        });
    });
    test('Status 404, Location not found', async () => {
      return request(app)
        .get('/loc/query/whatevercity')
        .expect(404)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(res.body.error.message).toBe('Not found');
          expect(res.body.error.code).toBe(404);
        });
    });
  });
  describe('Main endpoint (coordinates): /coords/:latitude,:longitude', () => {
    test('Status 200, Return JSON', async () => {
      const lon = -6.2603;
      const lat = 53.3498;
      return request(app)
        .get(`/coords/${lat},${lon}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(LocationHandlers.getCoords).toHaveBeenCalledWith(lat, lon);
          expect(WeatherHandlers.getWeatherArea).toHaveBeenCalledWith({
            type: 'Point',
            coordinates: [lon, lat]
          });
          expect(Object.keys(res.body).length).toBeGreaterThan(0);
          expect(res.body).toHaveProperty('weatherArea');
          expect(res.body).not.toHaveProperty('code');
          expect(res.body).not.toHaveProperty('list');
          expect(res.body).not.toHaveProperty('locations');
        });
    });
    test.each([
      [404, 'Not found'],
      [400, 'Invalid Area'],
      [500, 'reached openWeather limit']
    ])('Error, Response with status:  %i and message: %s', (code, message) => {
      let lon, lat;
      if (code === 400) {
        lon = -6.4;
        lat = 53.4;
      } else if (code === 404) {
        lon = -6.404;
        lat = 53.404;
      } else {
        lon = -6.5;
        lat = 53.5;
      }
      return request(app)
        .get(`/coords/${lat},${lon}`)
        .expect(code)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(res.body.error).toBeDefined();
          expect(res.body.error.code).toBe(code);
          expect(res.body.error.message).toBe(message);
          expect(LocationHandlers.getCoords).toHaveBeenCalledTimes(1);
          expect(WeatherHandlers.getWeatherArea).toHaveBeenCalledTimes(1);
        });
    });
  });
  describe('Main endpoint (coordinates): /query/:q', () => {
    test('Status 200, Return JSON', async () => {
      const query = 'Dublin';
      const lat = 53.3498;
      const lon = -6.2603;
      return request(app)
        .get(`/query/${query}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(LocationHandlers.getCity).toHaveBeenCalledWith(query);
          expect(WeatherHandlers.getWeatherArea).toHaveBeenCalledWith({
            type: 'Point',
            coordinates: [lon, lat]
          });
          expect(res.body).toBeArray();
          expect(res.body.length).toBeGreaterThan(0);
          const result = _.head(res.body);
          expect(result).toHaveProperty('weather');
          expect(result).not.toHaveProperty('code');
          expect(result).not.toHaveProperty('list');
          expect(result).not.toHaveProperty('locations');
          expect(result.message).toBe('This object comes from a mock function');
        });
    });
    test.each([
      [404, 'Not found'],
      [500, 'reached openWeather limit']
    ])('Error, Response with status:  %i and message: %s', (code, message) => {
      let query, callsWeatherArea;
      if (code === 404) {
        query = 'somewhere';
        callsWeatherArea = 0;
      } else {
        query = 'reject_by_weatherarea';
        callsWeatherArea = 1;
      }
      return request(app)
        .get(`/query/${query}`)
        .expect(code)
        .expect('Content-Type', /json/)
        .then(res => {
          expect(res.body.error).toBeDefined();
          expect(res.body.error.code).toBe(code);
          expect(res.body.error.message).toBe(message);
          expect(LocationHandlers.getCity).toHaveBeenCalledTimes(1);
          expect(WeatherHandlers.getWeatherArea).toHaveBeenCalledTimes(
            callsWeatherArea
          );
        });
    });
  });
});

describe('API CORS', () => {
  test.each([
    '/',
    '/weather/coords/53.3498,-6.2603',
    '/weather/query/dublin',
    '/loc/coords/53.3498,-6.2603',
    '/loc/query/dublin'
  ])('Allow only frontend origin and GET method, test route %s', route => {
    return request(app)
      .get(route)
      .expect('Access-Control-Allow-Origin', process.env.ORIGIN);
  });
});

describe('HELMET MIDDLEWARE', () => {
  test('helmet middleware is executed', () => {
    // Middleware should be called when requiring app
    expect(helmet).toHaveBeenCalled();
  });
});

describe('COMPRESSION MIDDLEWARE', () => {
  test('compression middleware is executed', () => {
    // Middleware should be called when requiring app
    expect(compression).toHaveBeenCalled();
  });
});

describe('API exceptions', () => {
  describe.each([
    '/weather/coords/53.3498,-6.2603',
    '/weather/query/dublin',
    '/loc/coords/53.3498,-6.2603',
    '/loc/query/dublin'
  ])('Correct route: %s, Incorrect verb', route => {
    test.each(['POST', 'PUT', 'DELETE'])('Verb: %s, Status: 400', verb => {
      return request(app)
        [verb.toLowerCase()](route)
        .expect(400)
        .expect('Content-Type', /json/)
        .then(res =>
          expect(res.body.error).toEqual({
            code: 400,
            route,
            method: verb,
            message: 'Bad request'
          })
        );
    });
  });
  describe.each([
    '/',
    '/abc',
    '/weather',
    '/weather/coords',
    '/weather/query',
    '/weather/query/abc/abc',
    '/weather/coords/abc',
    '/weather/coords/abc/abc',
    '/weather/coords/53.3498-6.2603',
    '/weather/coords/53.34986.2603',
    '/weather/coords/53.3498/-6.2603',
    '/weather/coords/53.3498/6.2603',
    '/loc',
    '/loc/coords',
    '/loc/query',
    '/loc/query/abc/abc',
    '/loc/coords/abc',
    '/loc/coords/abc/abc',
    '/loc/coords/53.3498-6.2603',
    '/loc/coords/53.34986.2603',
    '/loc/coords/53.3498/-6.2603',
    '/loc/coords/53.3498/6.2603'
  ])('Malformed route: %s', route => {
    test.each(['GET', 'POST', 'PUT', 'DELETE'])(
      'Verb: %s, Status: 400',
      verb => {
        return request(app)
          [verb.toLowerCase()](route)
          .expect(400)
          .expect('Content-Type', /json/)
          .then(res =>
            expect(res.body.error).toEqual({
              code: 400,
              route,
              method: verb,
              message: 'Bad request'
            })
          );
      }
    );
  });
});
