const mongoose = require('mongoose');
const app = require('./app.js');

const DB_PROTOCOL = process.env.DB_PROTOCOL;
const PORT = process.env.PORT;
const HOSTNAME = process.env.HOSTNAME;
const DB_PATH = process.env.DB_PATH;
const DB_PORT = process.env.DB_PORT;
const DB_NAME = process.env.DB_NAME;
const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;

/* Connect to DB */
//Protocol defines if is connecting to atlas or locally
let dbConnectOptions, dbUrl;
if (DB_PROTOCOL === 'mongodb') {
  dbConnectOptions = {
    bufferCommands: false,
    user: DB_USER,
    pass: DB_PASSWORD,
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  };
  dbUrl = `${DB_PROTOCOL}://${DB_PATH}:${DB_PORT}/${DB_NAME}`;
  console.log('Connecting to local DB: ', dbUrl);
} else {
  dbConnectOptions = {
    bufferCommands: false,
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  };
  dbUrl = `${DB_PROTOCOL}://${DB_USER}:${DB_PASSWORD}@${DB_PATH}/${DB_NAME}?retryWrites=true&w=majority`;
  console.log('Connecting to remote Atlas DB');
}

mongoose
  .connect(dbUrl, dbConnectOptions)
  .then(() => {
    console.log('Database is up, caching data');
    mongoose.connection.on('error', err => {
      console.error(
        'Unable to cache data, Database connection error:\n',
        err.message
      );
    });
    mongoose.connection.on('reconnected', () => {
      console.log('Database is up, caching data');
    });
    mongoose.connection.on('reconnectFailed', () => {
      console.warn('Unable to cache data');
      console.error(
        'DB reconnection failed, once solved the issue, restart the app to connect the DB'
      );
    });
  })
  .then(() =>
    app.listen(PORT, () => {
      console.log(`Server initialized on ${HOSTNAME}:${PORT}`);
    })
  )
  .catch(err => {
    console.error(
      'Unable to connect to the database, DB not initialized\n',
      err.message
    );
    console.log('Restore the app once the issue is solved');
  });
