const mongoose = require('mongoose');

module.exports = new mongoose.Schema({
  dt: { type: Date, required: true, min: 1000000000000 },
  sunrise: { type: Date, required: true, min: 1000000000000 },
  sunset: { type: Date, required: true, min: 1000000000000 },
  temp: {
    avg: { type: Number, required: true, min: 200, max: 345},
    feelsLike: { type: Number, required: true, min: 200, max: 345},
    min: { type: Number, required: true, min: 200, max: 345},
    max: { type: Number, required: true, min: 200, max: 345 },
  },
  main: [{
    main: { type: String, required: true },
    description: { type: String, required: true },
    icon: { type: String, required: true }
  }],
  clouds: { type: Number, min: 0, max: 100 }, //Units percentage
  wind: {
    speed:  { type: Number, min: 0 },
    deg: { type: Number, min: 0, max: 359.999 } // Units degrees, Note that 0 = 360
  },
  rain: { type: Number, min: 0},
  snow: { type: Number, min: 0}
});
