const mongoose = require('mongoose');
const Helpers = require('../handlers/helpers');
const weatherItem = require('./subdocuments/weather');

//TODO geometry should be unique
const weatherAreaSchema = new mongoose.Schema(
  {
    grid: {
      type: String,
      required: true,
      unique: true,
      validate: {
        validator: v => Helpers.isGridIndex(v),
        message: '{PATH} is not a vald grid index'
      }
    },
    list: { type: [weatherItem], required: true },
    updatedAt: { type: Date, required: true, default: new Date() }
  },
  { timestamps: true }
);

module.exports = mongoose.model('WeatherArea', weatherAreaSchema);
