const mongoose = require('mongoose');
const WeatherAreaModel = require('./weatherArea.js');
const weatherData = require('./__mocks__/weather-mock-data.json');
const Helpers = require('../handlers/helpers');

describe('Weather Area Model', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        bufferCommands: false
      },
      err => {
        if (err) throw new Error(err);
      }
    );
  });
  beforeEach(async () => {
    await WeatherAreaModel.deleteMany();
  });
  afterAll(async () => {
    await mongoose.disconnect();
  });

  test('Create and save weatherArea successfully', async () => {
    const weatherArea = new WeatherAreaModel(weatherData);
    await weatherArea.save();
    const doc = await WeatherAreaModel.findOne();
    expect(doc).toBeInstanceOf(WeatherAreaModel);
    expect(doc).toBeInstanceOf(mongoose.Document);
    expect(doc._id).toBeDefined();
  });

  test('Validate required fields', () => {
    const weatherArea = new WeatherAreaModel({
      list: null,
      updatedAt: null,
      grid: null
    });
    const e = weatherArea.validateSync();
    expect(e instanceof mongoose.Error.ValidationError).toBe(true);
    expect(e.errors).toHaveProperty('list');
    expect(e.errors).toHaveProperty('updatedAt');
    expect(e.errors).toHaveProperty('grid');
  });

  // Geometry validation
  test('Grid is validated using isGridIndex helper', () => {
    const isGridIndexSpy = jest.spyOn(Helpers, 'isGridIndex');
    const weatherArea = new WeatherAreaModel(weatherData);
    weatherArea.validateSync();
    expect(isGridIndexSpy).toHaveBeenCalledWith(weatherData.grid);
  });

  //Properties validation
  test.each([
    ['is valid (All properties)', {}, true],
    [
      'is valid (Required properties only)',
      { clouds: null, wind: null, rain: null, snow: null },
      true
    ],
    [
      'is invalid: forecasted time date (dt) is under minimum',
      { dt: 999999999999 },
      false
    ],
    ['is invalid: sunrise date is under minimum', { dt: 999999999999 }, false],
    ['is invalid: sunset date is under minimum', { dt: 999999999999 }, false],
    [
      'is invalid: Temperature avg minimum is 200K',
      { temp: { avg: 199.99, feelLike: 290, min: 290, max: 290 } },
      false
    ],

    [
      'is invalid: Temperature avg maximum is 345K',
      { temp: { avg: 345.01, feelLike: 290, min: 290, max: 290 } },
      false
    ],
    [
      'is invalid: Temperature feelsLike minimum is 200K',
      { temp: { avg: 290, feelLike: 199.99, min: 290, max: 290 } },
      false
    ],

    [
      'is invalid: Temperature feelsLike maximum is 345K',
      { temp: { avg: 290, feelLike: 345.01, min: 290, max: 290 } },
      false
    ],
    [
      'is invalid: Temperature min minimum is 200K',
      { temp: { avg: 290, feelLike: 290, min: 199.99, max: 290 } },
      false
    ],

    [
      'is invalid: Temperature min maximum is 345K',
      { temp: { avg: 290, feelLike: 290, min: 345.01, max: 290 } },
      false
    ],
    [
      'is invalid: Temperature max minimum is 200K',
      { temp: { avg: 290, feelLike: 290, min: 290, max: 199.99 } },
      false
    ],

    [
      'is invalid: Temperature max maximum is 345K',
      { temp: { avg: 290, feelLike: 290, min: 290, max: 345.01 } },
      false
    ],
    [
      'is invalid: Clouds is a percentage value (max 100%)',
      { clouds: 100.01 },
      false
    ],
    [
      'is invalid: Clouds is a percentage value (min 0%)',
      { clouds: -0.01 },
      false
    ],
    [
      'is invalid: wind speed cannot be negative',
      { wind: { deg: 1, speed: -0.01 } },
      false
    ],
    [
      'is invalid: wind degree cannot be negative',
      { wind: { deg: -1, speed: 25 } },
      false
    ],
    [
      'is invalid: wind degree cannot max is 359.999',
      { wind: { deg: 360, speed: 25 } },
      false
    ],

    ['is invalid: rain cannot be negative', { rain: -0.01 }, false],

    ['is invalid: snow cannot be negative', { snow: -0.01 }, false]
  ])('Object "{list: [weather]}" %s', (_a, data, isValid) => {
    const weatherItem = weatherData.list[0];
    const weatherArea = new WeatherAreaModel({
      grid: weatherData.grid,
      list: [{ ...weatherItem, ...data }]
    });
    const validation = weatherArea.validateSync();
    if (isValid) {
      expect(validation).toBeUndefined();
    } else {
      expect(validation instanceof mongoose.Error.ValidationError).toBe(true);
    }
  });

  test('updatedAt is automatically updated', async () => {
    const weatherArea = new WeatherAreaModel(weatherData);
    const beforeFirst = new Date();
    const {
      _id: idFirst,
      updatedAt: first,
      list: listFirst
    } = await weatherArea.save();
    const betweenBoth = new Date();
    const {
      _id: idSecond,
      updatedAt: second,
      list: listSecond
    } = await WeatherAreaModel.findByIdAndUpdate(
      idFirst,
      { list: [] },
      { new: true }
    );
    const afterSecond = new Date();
    expect(first >= beforeFirst && first <= betweenBoth).toBe(true);
    expect(second >= betweenBoth && second <= afterSecond).toBe(true);
    expect(idSecond).toEqual(idFirst);
    expect(listFirst).not.toEqual(listSecond);
  });
  test('Grid is indexed', async () => {
    const indexes = await WeatherAreaModel.collection.getIndexes({
      full: true
    });
    const names = indexes.map(val => val.name);
    expect(names).toEqual(
      expect.arrayContaining([expect.stringContaining('grid')])
    );
  });
  test('Grid is unique', async () => {
    const weather = weatherData.list[0];
    const weatherData2 = {
      ...weatherData,
      list: [
        {
          ...weather,
          dt: weather.dt * 2
        }
      ]
    };
    await WeatherAreaModel.create(weatherData);
    await expect(WeatherAreaModel.create(weatherData2)).rejects.toThrow(
      /duplicate/i
    );
  });
});
