const mongoose = require('mongoose');
const LocationModel = require('./location');
const locationData = require('./__mocks__/location-mock-data.json')[0];
const { featureTypes } = require('../config.json');

describe('Location Model', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      {
        bufferCommands: false,
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
      },
      err => {
        if (err) throw new Error(err);
      }
    );
  });
  beforeEach(async () => {
    await LocationModel.deleteMany();
  });
  afterAll(async () => {
    await mongoose.disconnect();
  });
  test('Create and save location successfully', async () => {
    const location = new LocationModel(locationData);
    const savedLocation = await location.save();
    expect(savedLocation._id).toBeDefined();
    expect(savedLocation.osmId).toBe(parseInt(locationData.osmId));
    expect(savedLocation.osmId).toBeDefined();
    expect(savedLocation.osmType).toBe(locationData.osmType);
    expect(savedLocation.osmType).toBeDefined();
    expect(savedLocation.name).toBe(locationData.name);
    expect(savedLocation.name).toBeDefined();
    expect(savedLocation.county).toBe(locationData.county);
    expect(savedLocation.county).toBeDefined();
    expect(savedLocation.displayName).toBe(locationData.displayName);
    expect(savedLocation.displayName).toBeDefined();
    expect(savedLocation.country).toBe(locationData.country);
    expect(savedLocation.country).toBeDefined();
    expect(savedLocation.importance).toBe(parseFloat(locationData.importance));
    expect(savedLocation.importance).toBeDefined();
    expect(savedLocation.polygonOsmId).toBe(
      parseInt(locationData.polygonOsmId)
    );
    expect(savedLocation.polygonOsmId).toBeDefined();
    expect(savedLocation.polygonOsmType).toBe(locationData.polygonOsmType);
    expect(savedLocation.polygonOsmType).toBeDefined();
    // Mongoose transform Arrays into CoreMongooseArray which make fail expect.toEqual
    // coordinates content is tested in validator test below
    expect(savedLocation.geometry).toHaveProperty('coordinates');
    expect(savedLocation.geometry.type).toBe(locationData.geometry.type);
  });
  test('Validate required/not-required fields', () => {
    const location = new LocationModel({
      type: null,
      name: null,
      displayName: null,
      county: null,
      country: null,
      osmId: null,
      osmType: null,
      importance: null,
      polygonOsmType: null,
      polygonOsmId: null,
      geometry: null
    });
    const e = location.validateSync();
    expect(e instanceof mongoose.Error.ValidationError).toBe(true);
    expect(e.errors).toHaveProperty('type');
    expect(e.errors).toHaveProperty('name');
    expect(e.errors).not.toHaveProperty('county');
    expect(e.errors).toHaveProperty('displayName');
    expect(e.errors).toHaveProperty('osmId');
    expect(e.errors).toHaveProperty('osmType');
    expect(e.errors).toHaveProperty('country');
    expect(e.errors).toHaveProperty('geometry');
    expect(e.errors).not.toHaveProperty('importance');
    expect(e.errors).not.toHaveProperty('polygonOsmType');
    expect(e.errors).not.toHaveProperty('polygonOsmId');
  });
  test('Validate cast to string type', async () => {
    const location = new LocationModel({
      ...locationData,
      name: 24,
      county: true,
      displayName: 35,
      polygonOsmType: 1234568,
      osmType: 1234568,
      country: 24.5
    });
    const savedLocation = await location.save();
    expect(savedLocation._id).toBeDefined();
    expect(savedLocation.name).toBe('24');
    expect(savedLocation.county).toBe('true');
    expect(savedLocation.displayName).toBe('35');
    expect(savedLocation.polygonOsmType).toBe('1234568');
    expect(savedLocation.osmType).toBe('1234568');
    expect(savedLocation.country).toBe('24.5');
  });
  test.each([...featureTypes, 'invalidString'])(
    'Validate enum values for "type" property. Value: %s',
    typeValue => {
      const location = new LocationModel({
        ...locationData,
        type: typeValue
      });
      const validation = location.validateSync();
      if (typeValue === 'invalidString') {
        expect(validation instanceof mongoose.Error.ValidationError).toBe(true);
        expect(validation.errors).toHaveProperty('type');
      } else {
        expect(validation).toBeUndefined();
      }
    }
  );
  test.each([
    [
      'is valid',
      'Polygon',
      [
        [
          [0, 0],
          [10, 0],
          [10, 10],
          [0, 10],
          [0, 0]
        ]
      ],
      true
    ],
    [
      'has invalid type',
      'Point',
      [
        [
          [0, 0],
          [10, 0],
          [10, 10],
          [0, 10],
          [0, 0]
        ]
      ],
      false
    ],
    ['has invalid coordinates', 'Polygon', [0, 0], false],
    [
      'has invalid (not closed) coordinates',
      'Polygon',
      [
        [
          [0, 0],
          [10, 0],
          [10, 10],
          [0, 10],
          [0, 5]
        ]
      ],
      false
    ]
  ])('Property "geometry" %s', (_a, type, coordinates, valid) => {
    const location = new LocationModel({
      ...locationData,
      geometry: { type, coordinates }
    });
    const validation = location.validateSync();
    if (valid) {
      expect(validation).toBeUndefined();
    } else {
      expect(validation instanceof mongoose.Error.ValidationError).toBe(true);
      expect(validation.errors).toHaveProperty('geometry');
    }
  });
  test('Indexes: Geometry: 2dsphere, {name, county, displayName country}: text, {osmType, OsmId} and {polygonOsmType, polygonOsmId}', async () => {
    const indexes = await LocationModel.collection.getIndexes({ full: true });
    const names = indexes.map(val => val.name);
    expect(names).toEqual(
      expect.arrayContaining([
        'geometry_2dsphere',
        'name_text_county_text_country_text_displayName_text',
        'osmType_1_osmId_1',
        'polygonOsmType_1_polygonOsmId_1'
      ])
    );
  });
});
