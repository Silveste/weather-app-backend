const mongoose = require('mongoose');
const GJV = require('geojson-validation');
const polygonSchema = require('./subdocuments/polygon');
const { featureTypes } = require('../config.json');

const locationSchema = new mongoose.Schema({
  type: {
    type: String,
    required: true,
    enum: featureTypes
  },
  name: { type: String, required: true },
  displayName: { type: String, required: true },
  county: { type: String, required: false },
  country: { type: String, required: true },
  importance: { type: Number, required: false },
  osmId: { type: Number, required: true },
  osmType: { type: String, required: true },
  /*
  The location document merges information about towns, cities etc. that usually have a Point geometry with a Polygon geometry
  The idea is that a city or town is not a point instead we try to store the real surface of the location.
  To get the most accurate polygon. The coordiantes that the point holds are used to retrieve the Polygon which usually is a feature type 'administrative'.
  polygonOsmId and polygonOsmType hold the osm_id and osm_type of the polygon feature where the geometry has been obtained
*/
  polygonOsmId: Number,
  polygonOsmType: String,
  polygonName: String,
  geometry: {
    type: polygonSchema,
    required: true,
    validate: {
      validator: GJV.isPolygon,
      message: '{PATH} is not a Polygon geoJson'
    }
  }
});

locationSchema.index({ geometry: '2dsphere' });
locationSchema.index({ osmType: 1, osmId: 1 });
locationSchema.index({ polygonOsmType: 1, polygonOsmId: 1 });
locationSchema.index({
  name: 'text',
  county: 'text',
  country: 'text',
  displayName: 'text'
});

module.exports = mongoose.model('Location', locationSchema);
