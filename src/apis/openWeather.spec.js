const axios = require('axios');
const openWeatherResponse = require('./__mocks__/openWeatherResponse.json');

const { getWeatherFromRemote } = require('./openWeather');

jest.setTimeout(20000);

describe.skip('openWeather API (Real call to the API, these test should be skipped by default)', () => {
  test('Returns 7 days of daily weather object', async () => {
    const lat = 53.3498;
    const lon = -6.2603;
    const result = await getWeatherFromRemote(lat,lon);
    expect(result).toBeArrayOfSize(7);
    result.forEach( val => {
      expect(val).toEqual(expect.objectContaining({
        dt: expect.any(Date),
        sunrise: expect.any(Date),
        sunset: expect.any(Date),
        temp: {
          avg: expect.any(Number),
          feelsLike: expect.any(Number),
          min: expect.any(Number),
          max: expect.any(Number),
        },
        main: expect.toIncludeAllMembers([expect.objectContaining({
          main: expect.any(String),
          description: expect.any(String),
          icon: expect.any(String)
        })])
      }));
    });
  });
});

describe('openWeather API (mocking http response)', () => {
  test('Returns required and optional keys (API response includes all posible keys)', async () => {
    const lat = 53.3498;
    const lon = -6.2603;
    jest.spyOn(axios, 'get').mockImplementation(() => Promise.resolve({ data: openWeatherResponse }));
    const result = await getWeatherFromRemote(lat,lon);
    expect(result).toBeArrayOfSize(7);
    result.forEach( val => {
      expect(val).toEqual(expect.objectContaining({
        dt: expect.any(Date),
        sunrise: expect.any(Date),
        sunset: expect.any(Date),
        temp: {
          avg: expect.any(Number),
          feelsLike: expect.any(Number),
          min: expect.any(Number),
          max: expect.any(Number),
        },
        main: expect.toIncludeAllMembers([expect.objectContaining({
          main: expect.any(String),
          description: expect.any(String),
          icon: expect.any(String)
        })]),
        clouds: expect.any(Number),
        wind: {
          speed: expect.any(Number),
          deg: expect.any(Number),
        },
        rain: expect.any(Number),
        snow: expect.any(Number)
      }));
    });
    axios.get.mockRestore();
  });
  test('Handle axios rejects throwing an error', async () => {
    const lat = 53.3498;
    const lon = -6.2603;
    jest.spyOn(axios, 'get').mockImplementation(() => Promise.reject(new Error('This is a test')));
    await expect(getWeatherFromRemote(lat,lon)).rejects.toThrow();
    axios.get.mockRestore();
  });
});
