const Nominatim = require('nominatim-geocoder');
const geocoder = new Nominatim({/* No options */}, {
  format: 'geojson',
  limit: 50,
  'accept-language': 'en',
  addressdetails: 1
});

const findRemoteCity = (query) => {
  return  geocoder.search({namedetails: 1, polygon_geojson: 0, countrycodes: 'ie,gb', q: query });
};

const findRemoteCoords = (lat,lon) => {
  return geocoder.reverse({polygon_geojson: 1, zoom: 12, lat, lon});
};

module.exports = { findRemoteCity, findRemoteCoords };
