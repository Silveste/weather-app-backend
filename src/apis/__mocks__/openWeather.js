const fromAPI = require('./openWeatherResponse.json');
const _ = require('lodash');

const _mapOpenweatherToLocal = fromAPI => {
  return {
    dt: new Date(fromAPI.dt * 1000),
    sunrise: new Date(fromAPI.sunrise * 1000),
    sunset: new Date(fromAPI.sunset * 1000),
    temp: {
      avg: fromAPI.temp.day,
      feelsLike: fromAPI.feels_like.day,
      min: fromAPI.temp.min,
      max: fromAPI.temp.max
    },
    main: fromAPI.weather.map(val => _.pick(val,['main', 'description', 'icon'])),
    clouds: fromAPI.clouds,
    rain: fromAPI.rain,
    snow: fromAPI.snow,
    wind: {
      speed: fromAPI.wind_speed,
      deg: fromAPI.wind_deg
    }
  };
};

const getWeatherFromRemote = jest.fn((lat,lon) => {
  const result = _.take(fromAPI.daily,7).map(day => _mapOpenweatherToLocal(day));
  return Promise.resolve(result);
});

module.exports = { getWeatherFromRemote };
