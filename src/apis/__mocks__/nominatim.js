// Returns the object for query 'carrickmacross monaghan'
const findRemoteCity = jest.fn((query) => {
  if (!query.toLowerCase().includes('carrickmacross')) {
    return { features: [] };
  }
  return{
    features: [{
      'type': 'Feature',
      'properties': {
        'place_id': 53887840,
        'osm_type': 'node',
        'osm_id': 4380804889,
        'type': 'town',
        'label': 'Carrickmacross, County Monaghan, A81 X242, Ireland',
        'name': 'Carrickmacross',
        'country': 'Ireland',
        'postcode': 'A81 X242',
        'state': 'A81 X242',
        'county': 'County Monaghan',
        'admin': {
          'level6': 'County Monaghan'
        }
      },
      'geometry': {
        'type': 'Point',
        'coordinates': [
          -6.7187084,
          53.977174
        ]
      }
    },
    {
      'type': 'Feature',
      'properties': {
        'place_id': 143799810,
        'osm_type': 'way',
        'osm_id': 232586876,
        'type': 'abandoned',
        'label': 'Carrickmacross, Inishkeen Bridge, Inishkeen Glebe, Inishkeen, Carrickmacross-Castleblaney Municipal District, County Monaghan, A91 H977, Ireland',
        'name': 'Carrickmacross',
        'country': 'Ireland',
        'postcode': 'A91 H977',
        'state': 'A91 H977',
        'county': 'County Monaghan',
        'city': 'Carrickmacross-Castleblaney Municipal District',
        'district': 'Inishkeen Glebe',
        'street': 'Inishkeen Bridge',
        'admin': {
          'level6': 'County Monaghan',
          'level8': 'Carrickmacross-Castleblaney Municipal District',
          'level9': 'Inishkeen',
          'level10': 'Inishkeen Glebe'
        }
      },
      'geometry': {
        'type': 'LineString',
        'coordinates': [
          [
            -6.5873399,
            54.0038993
          ],
          [
            -6.5872242,
            54.0039346
          ],
          [
            -6.5859475,
            54.0043468
          ],
          [
            -6.5836213,
            54.005129
          ],
          [
            -6.5823216,
            54.0055075
          ],
          [
            -6.5810163,
            54.0057149
          ],
          [
            -6.5803158,
            54.0057312
          ],
          [
            -6.5796175,
            54.0057316
          ],
          [
            -6.5788485,
            54.0056416
          ],
          [
            -6.5782022,
            54.0054721
          ]
        ]
      }
    },
    {
      'type': 'Feature',
      'properties': {
        'place_id': 139810609,
        'osm_type': 'way',
        'osm_id': 220211155,
        'type': 'abandoned',
        'label': 'Carrickmacross, Carrickmacross Bypass, Drummond Otra, Carrickmacross Rural, Carrickmacross-Castleblaney Municipal District, County Monaghan, A91 H977, Ireland',
        'name': 'Carrickmacross',
        'country': 'Ireland',
        'postcode': 'A91 H977',
        'state': 'A91 H977',
        'county': 'County Monaghan',
        'city': 'Carrickmacross-Castleblaney Municipal District',
        'district': 'Drummond Otra',
        'street': 'Carrickmacross Bypass',
        'admin': {
          'level6': 'County Monaghan',
          'level8': 'Carrickmacross-Castleblaney Municipal District',
          'level9': 'Carrickmacross Rural',
          'level10': 'Drummond Otra'
        }
      },
      'geometry': {
        'type': 'LineString',
        'coordinates': [
          [
            -6.7114498,
            53.9758845
          ],
          [
            -6.7077697,
            53.973003
          ],
          [
            -6.7070245,
            53.9725905
          ],
          [
            -6.7058696,
            53.9718734
          ],
          [
            -6.7051295,
            53.9713834
          ],
          [
            -6.7044287,
            53.9710069
          ],
          [
            -6.7040533,
            53.9708408
          ],
          [
            -6.7035735,
            53.9706899
          ],
          [
            -6.7010557,
            53.9701535
          ],
          [
            -6.6978027,
            53.9700538
          ],
          [
            -6.6944124,
            53.97018
          ],
          [
            -6.691065,
            53.9703062
          ],
          [
            -6.6857864,
            53.970787
          ],
          [
            -6.6772878,
            53.9723108
          ],
          [
            -6.6691088,
            53.9739002
          ],
          [
            -6.6653751,
            53.9746069
          ],
          [
            -6.6632515,
            53.9749784
          ],
          [
            -6.6597418,
            53.9756523
          ],
          [
            -6.6559273,
            53.9763657
          ],
          [
            -6.65361,
            53.9765803
          ],
          [
            -6.6515657,
            53.9764718
          ],
          [
            -6.6493434,
            53.9761012
          ],
          [
            -6.6478551,
            53.9757807
          ],
          [
            -6.6463894,
            53.9756299
          ],
          [
            -6.6441313,
            53.9755993
          ],
          [
            -6.6422155,
            53.9758206
          ],
          [
            -6.6404764,
            53.9762665
          ],
          [
            -6.6354467,
            53.9780046
          ],
          [
            -6.6268551,
            53.9798457
          ],
          [
            -6.6225721,
            53.9810078
          ],
          [
            -6.6201689,
            53.9819164
          ],
          [
            -6.6151031,
            53.9841309
          ],
          [
            -6.6117533,
            53.9858467
          ],
          [
            -6.6107213,
            53.9864542
          ],
          [
            -6.6097361,
            53.9871723
          ],
          [
            -6.6087017,
            53.9880443
          ],
          [
            -6.6076871,
            53.9889677
          ],
          [
            -6.6067255,
            53.9900376
          ],
          [
            -6.6049339,
            53.9923627
          ],
          [
            -6.6017153,
            53.9962479
          ],
          [
            -6.5992644,
            53.9990454
          ]
        ]
      }
    }
    ]
  }
;
});

// Returns the object for '53.977174, -6.7187084'
const findRemoteCoords = jest.fn((lat, lon) => {
  if (lat === -1 && lon === -1) {
    return { error: 'unable to geocode' };
  }
  return {
    features: [{
      'type': 'Feature',
      'properties': {
        'place_id': 235885741,
        'osm_type': 'relation',
        'osm_id': 5680367,
        'place_rank': 20,
        'category': 'boundary',
        'type': 'administrative',
        'importance': 0.25,
        'addresstype': 'boundary',
        'name': 'Drummond Otra',
        'display_name': 'Drummond Otra, Carrickmacross Rural, Carrickmacross-Castleblaney Municipal District, County Monaghan, Ireland',
        'address': {
          'suburb': 'Drummond Otra',
          'city_district': 'Carrickmacross Rural',
          'region': 'Carrickmacross-Castleblaney Municipal District',
          'county': 'County Monaghan',
          'country': 'Ireland',
          'country_code': 'ie'
        }
      },
      'bbox': [
        -6.7199651,
        53.9657932,
        -6.6925922,
        53.9792992
      ],
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [-6.7300, 53.9600],
            [-6.6800, 53.9600],
            [-6.6800, 53.9800],
            [-6.7300, 53.9800],
            [-6.7300, 53.9600],
          ]
        ]
      }
    }]
  };
});
module.exports = {
  findRemoteCity,
  findRemoteCoords
};
