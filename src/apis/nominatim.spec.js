const GJV = require('geojson-validation');
const { findRemoteCity, findRemoteCoords } = require('./nominatim');

jest.setTimeout(20000);

describe.skip('Nominatim API (Real call to the API, these test should be skipped by default)', () => {
  describe('Search by query', () => {
    test('Returns a FeatureCollection', async () => {
      const query = 'Shercock';
      const result = await findRemoteCity(query);
      expect(GJV.isFeatureCollection(result)).toBe(true);
      expect(result.features.length).toBeGreaterThan(0);
    });
    test('Returns an empty FeatureCollection when no result available', async () => {
      const query = 'a2we34rfdsgt56';
      const result = await findRemoteCity(query);
      expect(GJV.isFeatureCollection(result)).toBe(true);
      expect(result.features.length).toBe(0);
    });
  });
  describe('Search by coords', () => {
    test('Returns a FeatureCollection', async () => {
      const lat = 53.8472;
      const lon = -7.0275;
      const result = await findRemoteCoords(lat, lon);
      expect(GJV.isFeatureCollection(result)).toBe(true);
      expect(result.features.length).toBe(1);
    });
    test('Returns error when no result available', async () => {
      const lat = 53.9819;
      const lon = -14.318;
      const result = await findRemoteCoords(lat, lon);
      expect(result).toHaveProperty('error', 'Unable to geocode');
    });
  });
});
