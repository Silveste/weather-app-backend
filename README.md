# Weather App (Backend)

Repository that provides a backend for the Weather App (Frontend) to access to [OpenWeather](https://openweathermap.org) and [Nominatim](https://nominatim.org) APIs

It uses [NodeJS](https://nodejs.org) and [Express](https://expressjs.com/) among other libraries.

## Endpoints

### /weather/coords/:lat,:lon

Search for a location (inside map boundaries set in config.json) which contains the point with the `:lat` and `:lon` parameters

- **lat:** Number that indicates the latitude (degrees in decimal notation) where positive is North and negative is South

- **lon:** Number that indicates the longitude (degrees in decimal notation) where positive is East and negative is West.

Returns a [WeatherArea](#weatherarea) that contains the point represented by :lat and :lon

### /weather/query/:q

Search for a location (inside map boundaries set in config.json) that matches the query `:q` (which is decoded using [decodeURI](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/decodeURI))

Returns the [WeatherArea](#weatherarea) of the location with the highest score of the search query

### /loc/coords/:lat,:lon

Search for a location (inside map boundaries set in config.json) which contains the point with the `:lat` and `:lon` parameters

- **lat:** Number that indicates the latitude (degrees in decimal notation) where positive is North and negative is South

- **lon:** Number that indicates the longitude (degrees in decimal notation) where positive is East and negative is West.

The point must be between the map edges defined in config.json

Returns an Array of [Locations](#location) however the array length is always 1

### /loc/query/:q

Search for a location (inside map boundaries set in config.json) that matches the query `:q` (which is decoded using [decodeURI](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/decodeURI))

Returns an Array of [Locations](#location).

### /query/:q

Search for a location (inside map boundaries set in config.json) that matches the query `:q` (which is decoded using [decodeURI](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/decodeURI))

Returns an array of [location](#location) objects that represents every location found. Every location has an added key "weather" that contains the [WeatherArea](#weatherarea) object. Note that different locations could have the same WeatherArea.

### /coords/:lat,:lon

Search for a location (inside map boundaries set in config.json) which contains the point with the `:lat` and `:lon` parameters

- **lat:** Number that indicates the latitude (degrees in decimal notation) where positive is North and negative is South

- **lon:** Number that indicates the longitude (degrees in decimal notation) where positive is East and negative is West.

The point must be between the map edges defined in config.json

Returns a modified [Locations](#location) with and added key: "weatherArea" that contains a [WeatherArea](#weatherarea) where the location is contained.

## Location

The description object contains the following keys

- id: Id of the location (Nominatim osmType and osmId)
- country: String, with the country name
- country: String, with the county name
- displayName: String, with long description of the location
- name: String, with the name of the location
- type: String, value of principal tag defining the location
- importance: Number[¹], with Nominatim importance score. See [here](https://lists.openstreetmap.org/pipermail/geocoding/2013-August/000916.html)
- geometry: GeoJSON geometry object, type Polygon with only one LinearRing that describes the area of the location.
- score: Number[¹], with database search score which is calculatedby dividing [the mongodb score](https://docs.mongodb.com/manual/tutorial/control-results-of-text-search/) between the query number of words. The mongo db score is calculated using the fields name, county, country and displayName with no weights applied. **The score can be null**
  which indicates that the location has been retrieved from nomiatim API instead the DB.

[¹]: The API response is sent in JSON. Therefore all types are String. However this type can be cast to a Number

## WeatherArea

To limit the number of calls to openweather API (as the project uses a free account) the map is divided in areas of 10x10 km aprox. A WeatherArea describes the weather in such area.

The weather is obtained using openweather API with the coordinates that are in the WeatherArea center of mass.

A WeatherArea is an object with the following keys:

- geometry: GeoJSON geometry type Polygon with one linear ring that describes the area.
- list: Array of objects of length = 7 with daily weather information. Every item represents a day. Every item has the following keys:
  - dt: Date that represents the day of the current weather. Indicated in UTC time
  - sunrise: Date that represents the sunrise time. Indicated in UTC time
  - sunset: Date that represents the sunset time. Indicated in UTC time
  - temp: Object with information about daily temperature. Contains the following keys:
    - avg: Number, daily average temperature in Kelvin degrees
    - feelsLike: Number, subjective daily average temperature in Kelvin degrees
    - min: Number, minimum daily temperature in Kelvin degrees
    - max: Number, maximum daily temperature in Kelvin degrees
  - main: Object that contains weather descriptions in English. Contains the following keys
    - main: String with a short weather description. indicating only the weather parameter.
    - description: String with a short weather description
    - icon: String with [icon Id](https://openweathermap.org/weather-conditions#How-to-get-icon-URL)
  - clouds: Number with percentage of cloud coverage. Can also be undefined or null
  - wind: Object with wind information. Can also be undefined or null
    - speed: Number, wind speed in metres per second.
    - deg: Number, Wind direction, degrees (meteorological)
  - rain: Precipitation volume, mm. Can also be undefined or null
  - snow: Snow volume, mm. Can also be undefined or null

Note that all primitive types are strings as per JSON definition. However when indicated Number the value can be casted into that data type.

## Notes

- To use default mock env vars rename "public-env" to ".env" (Note that some vars such as keys might be missing).

- To test any file using jest CLI use this command:

```bash
$> clear && jest src/mySpec.spec.js --setupFiles dotenv/config
```

## Todos

- Rewrite location handler spec and location handler, currently is messy
